from setuptools import setup, find_packages

VERSION = '1.0.0' 
DESCRIPTION = 'Test and measurement instruments support library'
LONG_DESCRIPTION = 'Library supports custom T&M instruments build by Woj.Rus. (DAQs, Vref sources etc.)'

setup(
    name = "LabIO",
    version = VERSION,
    author = "Woj. Rus.",
    author_email = "rwxrwx@interia.pl",
    url = r"https://gitlab.com/wojrus-projects/python/labio",
    description = DESCRIPTION,
    long_description = LONG_DESCRIPTION,
    packages = find_packages(),
    install_requires = [
        'h5py>=3.10.0',
        'matplotlib>=3.8.2',
        'numpy>=1.26.2',
        'progressbar2>=4.2.0',
        'psutil>=5.9.6',
        'pyserial>=3.5',
        'scipy>=1.11.4'
    ],
    python_requires = ">=3.11",
    license = 'MIT',
    keywords = ['DAQ', 'T&M', 'USB', 'measurement'],
    classifiers = [
        "Development Status :: 3 - Alpha",
        "Environment :: Console",
        "Intended Audience :: Developers",
        "Intended Audience :: Science/Research",
        "License :: OSI Approved :: MIT License",
        "Natural Language :: English",
        "Operating System :: Microsoft :: Windows :: Windows 10",
        "Operating System :: Microsoft :: Windows :: Windows 11",
        "Operating System :: POSIX :: Linux",
        "Programming Language :: Python :: 3 :: Only",
        "Topic :: Scientific/Engineering",
        "Topic :: Software Development :: Embedded Systems"
    ]
)
