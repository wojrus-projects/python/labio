# LabIO

LabIO jest zbiorem bibliotek i aplikacji w języku Python 3.12+ przeznaczonych do obsługi urządzeń pomiarowych, kontrolnych i do akwizycji danych.

Obsługiwane typy urządzeń:
- [**DAQ_M01**](https://gitlab.com/wojrus-projects/altium-designer/daq_m01_v1_0) (USB DAQ: 6x voltage inputs, 24 bit, differential, non isolated)
- **DAQ_M02** (USB DAQ: 2x current inputs, 16 bit, isolated)
- [**VREF_M01**](https://gitlab.com/wojrus-projects/altium-designer/vref_m01_v1_1) (USB voltage reference: 1x voltage output, 16 bit, non isolated)
- [**RELAY_USB_M01**](https://gitlab.com/wojrus-projects/altium-designer/relay_usb_m01_v1_0) (USB relay module: 8x NO/NC outputs, isolated)

Dostępne [aplikacje](Application/README.md):
- `daq_logger.py` - odczytanie danych z DAQ i zapisanie do pliku HDF5 do dalszego przetwarzania w MATLAB, Octave itp.
- `daq_plot.py` - odczytanie danych z DAQ i wyświetlenie wykresów czasowych i FFT.

# Instalacja (Windows 10)

Poniższe czynności wykonywać w katalogu w którym znajduje się bieżący plik `README.md`.

Instalacja z pakietu w wersji binarnej:

```bat
pip install Package\labio-1.0.0.tar.gz
```

Alternatywna instalacja z plików źródłowych:

```bat
pip install .
```

Sprawdzenie instalacji:

```bat
pip show labio
```

Usunięcie pakietu:

```bat
pip uninstall labio
```

# Przykład użycia biblioteki

```python
from LabIO import vref_m01

device = vref_m01.VREF_M01()
device.connect("COM3")

device_status, device_identification = device.get_device_info()
print(device_status.to_string())
print(device_identification.to_string())

device.set_voltage(5.0)

device.disconnect()
```

# Licencja

MIT
