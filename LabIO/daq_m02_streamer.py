"""
DAQ_M02 streamer.
"""

import threading
import queue
import logging
import time

from . import daq
from . import daq_m02
from . import host_protocol

class DAQ_M02_Streamer(daq_m02.DAQ_M02):
    """
    DAQ_M02 streamer.\n
    This class is intended to inherit and define custom DAQ_M02 streaming devices.
    """
    def __init__(self) -> None:
        super().__init__()
        self.__clear()
        
        self.set_command_answer_callback(daq_m02.CommandCode.Acquisition_Push_Data,
                                         self.__command_answer_callback)
    def __clear(self) -> None:
        """
        Clear object.
        """
        self.__host_protocol_fifo: queue.Queue = queue.Queue(maxsize=0)
        self.__host_protocol_exception: (host_protocol.HostProtocolException | None) = None
        
        self.__callback_on_samples: (daq.CallbackOnSamples | None) = None
        self.__thread_worker_is_enabled = False
        
        self.__acquisition_is_complete = False
        self.__sample_counter = 0
        
        self.__gain_coefficients: tuple[float, ...] = ()

    def start_streaming(self,
                        trigger_mode: daq_m02.TriggerMode,
                        adc_current_range: daq_m02.CurrentRange,
                        adc_sampling_frequency: int,
                        adc_sample_count_single_acquisition: int,
                        adc_sample_count_total: int,
                        callback_on_samples: daq.CallbackOnSamples) -> None:
        """
        Process continuous data acquisition.\n
        Received samples are pushed to application via callback.
        
        :param trigger_mode:                            Set trigger input mode: 0=disabled, 1=rising edge, 2=falling edge, 3=both edges
        :param adc_current_range:                       Set current range: 1=5mA, 2=50mA, 3=500mA, 4=5A
        :param adc_sampling_frequency:                  Set sampling frequency, range: 1 Hz to 250 kHz
        :param adc_sample_count_single_acquisition:     Number of samples in single triggered acquisition, range: 1 to 2**64-1
        :param adc_sample_count_total:                  Number of samples in all triggered acquisitions, range: 1 to 2**64-1
        :param callback_on_samples:                     Callback on received samples (called from private thread).\n
        Callback parameters:\n
          * ADC samples (numpy.ndarray)
          * ADC data sequence counter (int)
        """
        self.__clear()
        
        trigger_mode = daq_m02.TriggerMode(trigger_mode)
        adc_current_range = daq_m02.CurrentRange(adc_current_range)
        adc_sampling_frequency = int(adc_sampling_frequency)
        adc_sample_count_single_acquisition = int(adc_sample_count_single_acquisition)
        adc_sample_count_total = int(adc_sample_count_total)
        
        if adc_sampling_frequency < 1:
            raise ValueError("Invalid sampling frequency")
        
        if adc_sample_count_single_acquisition < 1:
            raise ValueError("Invalid sample count (single)")
        
        if adc_sample_count_total < adc_sample_count_single_acquisition:
            raise ValueError("Invalid sample count (total)")
            
        if trigger_mode == daq_m02.TriggerMode.Disabled:
            adc_sample_count_total = adc_sample_count_single_acquisition

        #
        # Check calibration.
        #
        acquisition_status = self.acquisition_get_status()
        
        if not acquisition_status.calibration_is_valid:
            logging.warning("ADC calibration is invalid")
        
        #
        # Start measurement.
        #
        self.__sample_counter = 0
        self.__callback_on_samples = callback_on_samples
        self.__host_protocol_exception = None
        self.__acquisition_is_complete = False

        self.__gain_coefficients = self.acquisition_start(trigger_mode,
                                                          adc_current_range,
                                                          adc_sampling_frequency,
                                                          adc_sample_count_single_acquisition)
        
        thread = threading.Thread(target=self.__adc_data_worker, name='ADC data worker', daemon=True)
        thread.start()

        #
        # Wait for acquisition completion or host protocol exception.
        #
        while (not self.__acquisition_is_complete) and self.__thread_worker_is_enabled:
            time.sleep(0.1)

        #
        # Protocol return unrecovable error.
        # At this point USB serial port is disabled.
        #
        if self.__host_protocol_exception:
            raise self.__host_protocol_exception

        #
        # Stop acquisition.
        #
        self.acquisition_stop()

        #
        # Check DAQ buffer overflow.
        #
        acquisition_status = self.acquisition_get_status()
        
        assert acquisition_status.acquisition_state == daq_m02.SamplerState.Stop
        
        if trigger_mode == daq_m02.TriggerMode.Disabled:
            assert acquisition_status.counter_trigger == 0
            assert acquisition_status.counter_sample_acquisition == acquisition_status.counter_sample_total
            assert acquisition_status.counter_sample_acquisition == adc_sample_count_single_acquisition
            assert acquisition_status.counter_buffer_overflow_acquisition == acquisition_status.counter_buffer_overflow_total
        else:
            assert acquisition_status.counter_trigger > 0
            assert acquisition_status.counter_sample_acquisition <= acquisition_status.counter_sample_total
            assert acquisition_status.counter_sample_acquisition <= adc_sample_count_single_acquisition
            assert acquisition_status.counter_buffer_overflow_acquisition <= acquisition_status.counter_buffer_overflow_total
            
        assert self.__sample_counter <= (acquisition_status.counter_sample_total - acquisition_status.counter_buffer_overflow_total)
        
        if acquisition_status.counter_buffer_overflow_total > 0:
            logging.warning("ADC buffer overflow ({} samples lost)"
                .format(acquisition_status.counter_buffer_overflow_total))
    
    def __command_answer_callback(self, protocol_object: host_protocol.PDU | host_protocol.HostProtocolException) -> None:
        """
        PDU/exception FIFO producer.\n
        This function is executed in host protocol PDU parser thread.
        
        :param protocol_object:     Answer PDU from device or host protocol exception.
        """
        # If protocol exception then clear application Rx FIFO.
        if isinstance(protocol_object, host_protocol.HostProtocolException):
            with self.__host_protocol_fifo.mutex:
                self.__host_protocol_fifo.queue.clear()
        
        self.__host_protocol_fifo.put(protocol_object, block=True, timeout=None)
    
    def __adc_data_worker(self) -> None:
        """
        PDU/exception FIFO consumer and data worker.
        """
        self.__thread_worker_is_enabled = True
        
        logging.info("ADC data worker start")
        
        while not self.__acquisition_is_complete:
            try:
                # Get PDU with ADC data or host protocol exception.
                
                queue_item = self.__host_protocol_fifo.get(block=True, timeout=0.1)
                
                if isinstance(queue_item, host_protocol.PDU):
                    
                    # Process ADC data from PDU.
                    
                    pdu = queue_item
                    samples = self.pdu_to_samples(pdu, self.__gain_coefficients)
                    sequence_counter = self.get_adc_data_sequence_counter(pdu)
                    self.__sample_counter += len(samples)
                    if self.__callback_on_samples:
                        self.__callback_on_samples(samples, sequence_counter)
                    
                    if pdu.header.status == daq_m02.StatusCode.Acquisition_Data_End:
                        self.__acquisition_is_complete = True
                        
                elif isinstance(queue_item, host_protocol.HostProtocolException):
                    
                    # On communication error save error and gracefully finish ADC data thread.
                    
                    self.__host_protocol_exception = queue_item
                    self.__acquisition_is_complete = True
                else:
                    assert False

            except queue.Empty:
                pass
                
        logging.info("ADC data worker stop")

        self.__thread_worker_is_enabled = False
