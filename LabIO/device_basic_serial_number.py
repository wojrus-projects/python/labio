"""
Device with minimal functionality + serial number.
"""

from . import device_basic
from . import device_info

class DeviceBasicSerialNumber(device_basic.DeviceBasic):
    """
    Device with minimal functionality + serial number.\n
    This class is intended to inherit and define custom devices.
    """
    def __init__(self) -> None:
        super().__init__()

    def set_serial_number(self, serial_number: int) -> None:
        """
        Set device S/N.
        
        :param serial_number:    Serial number: 0 to 2**32-1
        """
        if (serial_number < device_info.DeviceIdentification.SerialNumberMin) or \
            (serial_number > device_info.DeviceIdentification.SerialNumberMax):
            raise ValueError("Invalid S/N")
        
        flash_data = bytearray(serial_number.to_bytes(device_info.DeviceIdentification.SerialNumberSize, byteorder='little', signed=False))
        
        self.memory_erase(memory_id=device_basic.MemoryID.Configuration_Factory,
                          erase_offset=0,
                          erase_size=len(flash_data))
        
        write_size = self.memory_write(memory_id=device_basic.MemoryID.Configuration_Factory,
                                       write_offset=0,
                                       write_data=flash_data)

        if write_size != device_info.DeviceIdentification.SerialNumberSize:
            raise ValueError("Invalid write size")
