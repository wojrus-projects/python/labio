"""
DAQ_M01 basic functionality.
"""

import enum
import struct
import ctypes

import numpy

from . import host_protocol
from . import device_basic
from . import daq
from . import calibration_container

ADC_CHANNEL_COUNT = 6
ADC_CHANNEL_STATE_BITMASK = 0x3F
ADC_SAMPLE_SIZE = 3
ADC_GAIN_COEFFICIENT_SIZE = 4
ADC_CALIBRATION_DATA_SIZE = 433

PGA_GAIN_MIN = 1
PGA_GAIN_MAX = 128

pga_gains = (1, 2, 4, 8, 16, 32, 64, 128)

class CommandCode(enum.IntEnum):
    """
    Device specific commands (range 128-255).
    """
    Acquisition_Start = 128
    Acquisition_Stop = 129
    Acquisition_Get_Status = 130
    Acquisition_Push_Data = 131
    Calibration_Process = 132
    Calibration_Data_Read = 133
    Calibration_Data_Write = 134

class StatusCode(enum.IntEnum):    
    """
    Device specific codes (range 128-255).
    """
    Acquisition_Data_Begin = 128
    Acquisition_Data_Continue = 129
    Acquisition_Data_End = 130
    ADC_Calibration_Offset_Error = 131
    ADC_Calibration_Invalid_Input_Voltage = 132

class ADC_State(enum.IntEnum):
    Idle = 0
    Acquisition = 1
    Completed = 2
    Stop = 3
    
class CalibrationStage(enum.IntEnum):
    Invalid = 0
    Stage_1_Offset_Vin_Zero = 1
    Stage_2_Gain_Vin_Positive = 2
    Stage_3_Gain_Vin_Negative = 3

class AcquisitionStatus:
    """
    Acquisition status
    """
    def __init__(self) -> None:
        self.calibration_is_valid = False
        self.adc_state = ADC_State.Idle
        self.counter_sample = 0
        self.counter_buffer_overflow = 0
    
    def from_bytes(self, data: bytearray) -> None:
        self.calibration_is_valid = bool((data[0] >> 0) & 0x1)
        self.adc_state = ADC_State((data[0] >> 1) & 0x3)
        self.counter_sample = int.from_bytes(data[1 : 1 + 8], byteorder='little', signed=False)
        self.counter_buffer_overflow = int.from_bytes(data[9 : 9 + 8], byteorder='little', signed=False)
        
    def to_string(self) -> str:
        return "calibration: {}, state: {}, samples: {}, overflows: {}".format(
            int(self.calibration_is_valid),
            self.adc_state.name,
            self.counter_sample,
            self.counter_buffer_overflow)
    
    @staticmethod
    def get_length() -> int:
        return (1 + 8 + 8)

class DeviceException(Exception):
    pass

class DAQ_M01(daq.DAQ):
    """
    DAQ_M01 Data Acquisition Module.\n
    This class is intended to inherit and define custom DAQ_M01 devices.
    """
    def __init__(self) -> None:
        super().__init__()

    def acquisition_start(self,
                          adc_sampling_frequency: int,
                          adc_sample_count_max: int,
                          adc_channel_state: list[int],
                          adc_pga_gain: list[int]) -> tuple[float, ...]:
        """
        Start data acquisition.\n
        This function only trigger acquisition process in DAQ.\n
        Application must register callback to receive PDUs with raw data from DAQ:\n
        ``self.set_command_answer_callback(daq_m01.CommandCode.Acquisition_Push_Data, self.adc_data_callback)``\n
        In callback application must extract samples from PDU payload.\n
        Processing should use dedicated thread.\n
        Example code is in ``class DAQ_M01_Streamer``.
        
        :param adc_sampling_frequency:    Set sampling frequency, available: 250, 500, 1000, 2000, 4000, 8000, 16000, 32000 Hz
        :param adc_sample_count_max:      Number of samples in single acquisition, range: 1 to 2**64-1
        :param adc_channel_state:         ADC channels state: 0=disable, 1=enable
        :param adc_pga_gain:              Set PGA gain for channels 1-6: 1, 2, 4, 8, 16, 32, 64, 128
        
        :return:                          Gain coefficients (V/sample) for enabled channels.
        """        
        pdu = host_protocol.PDU()
        pdu.header.pdu_type = host_protocol.PDU_Type.Command
        pdu.header.sequence_counter = self.host_protocol.get_command_sequence_counter()
        pdu.header.command = CommandCode.Acquisition_Start
        pdu.header.status = 0
        pdu.payload = bytearray(4 + 8 + 1 + 6)
        
        adc_sampling_frequency = int(adc_sampling_frequency)
        adc_sample_count_max = int(adc_sample_count_max)
        
        if adc_sampling_frequency < 250:
            raise ValueError("Invalid sampling frequency")
        
        if adc_sample_count_max < 1:
            raise ValueError("Invalid sample count")
        
        if (len(adc_channel_state) != ADC_CHANNEL_COUNT) or \
            (len(adc_pga_gain) != ADC_CHANNEL_COUNT):
            raise ValueError("Invalid length")
        
        adc_channel_state_bitmap = 0
        adc_channel_counter = 0
        
        for channel_index, channel_state in enumerate(adc_channel_state):
            if channel_state:
                adc_channel_state_bitmap |= (1 << channel_index)
                adc_channel_counter += 1
        
        if adc_channel_counter == 0:
            raise ValueError("All channels are disabled")
        
        i = 0
        pdu.payload[i : i + 4] = adc_sampling_frequency.to_bytes(4, byteorder='little', signed=False)
        i += 4
        pdu.payload[i : i + 8] = adc_sample_count_max.to_bytes(8, byteorder='little', signed=False)
        i += 8
        pdu.payload[i] = adc_channel_state_bitmap
        i += 1
        
        for gain_index, gain_value in enumerate(adc_pga_gain):
            pdu.payload[i + gain_index] = gain_value
        
        self.send_command_pdu(pdu)
        
        answer_pdu = self.receive_answer_pdu()
        
        gain_coefficients = ()
        
        if answer_pdu.header.status == device_basic.StatusCode.Success:
            self.__check_answer_pdu(answer_pdu,
                                    CommandCode.Acquisition_Start,
                                    adc_channel_counter * ADC_GAIN_COEFFICIENT_SIZE)
            
            gain_coefficients = tuple(struct.unpack('f' * adc_channel_counter, answer_pdu.payload))
        else:
            self.__check_answer_pdu(answer_pdu, CommandCode.Acquisition_Start, 0)
        
        return gain_coefficients
    
    def acquisition_stop(self) -> None:
        """
        Stop ADC acquisition.
        """
        pdu = host_protocol.PDU()
        pdu.header.pdu_type = host_protocol.PDU_Type.Command
        pdu.header.sequence_counter = self.host_protocol.get_command_sequence_counter()
        pdu.header.command = CommandCode.Acquisition_Stop
        pdu.header.status = 0
        
        self.send_command_pdu(pdu)
        
        answer_pdu = self.receive_answer_pdu()
        self.__check_answer_pdu(answer_pdu, CommandCode.Acquisition_Stop, 0)
    
    def acquisition_get_status(self) -> AcquisitionStatus:
        """
        Get acquisition status.
        
        :return:     Acquisition status.
        """
        pdu = host_protocol.PDU()
        pdu.header.pdu_type = host_protocol.PDU_Type.Command
        pdu.header.sequence_counter = self.host_protocol.get_command_sequence_counter()
        pdu.header.command = CommandCode.Acquisition_Get_Status
        pdu.header.status = 0
        
        self.send_command_pdu(pdu)
        
        answer_pdu = self.receive_answer_pdu()
        self.__check_answer_pdu(answer_pdu, CommandCode.Acquisition_Get_Status, AcquisitionStatus.get_length())
        
        acquisition_status = AcquisitionStatus()
        acquisition_status.from_bytes(answer_pdu.payload)
        
        return acquisition_status
        
    def calibration_process_offset(self) -> None:
        """
        Process ADC offset calibration.
        """
        pdu = host_protocol.PDU()
        pdu.header.pdu_type = host_protocol.PDU_Type.Command
        pdu.header.sequence_counter = self.host_protocol.get_command_sequence_counter()
        pdu.header.command = CommandCode.Calibration_Process
        pdu.header.status = 0
        pdu.payload = bytearray(1)
        
        pdu.payload[0] = CalibrationStage.Stage_1_Offset_Vin_Zero
        
        self.send_command_pdu(pdu)
        
        answer_pdu = self.receive_answer_pdu()
        self.__check_answer_pdu(answer_pdu, CommandCode.Calibration_Process, 0)
        
    def calibration_process_gain_positive(self,
                                          adc_channel_state: list[int],
                                          adc_pga_gain: int,
                                          referenceVoltagePositive_V: float) -> None:
        """
        Process ADC gain calibration (positive measurement).
        
        :param adc_channel_state:               ADC channels state: 0=disable, 1=enable
        :param adc_pga_gain:                    Set same PGA gain for enabled channels: 1, 2, 4, 8, 16, 32, 64, 128
        :param referenceVoltagePositive_V:      Measured reference voltage (V), positive polarization (differential), range 1 mV to 1 kV.
        """
        if len(adc_channel_state) != ADC_CHANNEL_COUNT:
            raise ValueError("Invalid length")
        
        pdu = host_protocol.PDU()
        pdu.header.pdu_type = host_protocol.PDU_Type.Command
        pdu.header.sequence_counter = self.host_protocol.get_command_sequence_counter()
        pdu.header.command = CommandCode.Calibration_Process
        pdu.header.status = 0
        pdu.payload = bytearray(1 + 1 + 1 + 4)
        
        pdu.payload[0] = CalibrationStage.Stage_2_Gain_Vin_Positive
        
        adc_channel_state_bitmap = 0
        
        for channel_index, channel_state in enumerate(adc_channel_state):
            if channel_state:
                adc_channel_state_bitmap |= (1 << channel_index)
        
        pdu.payload[1] = adc_channel_state_bitmap
        pdu.payload[2] = adc_pga_gain
        
        referenceVoltagePositive_V = float(referenceVoltagePositive_V)
        fmt = struct.Struct('<f')
        pdu.payload[3 : 3 + 4] = fmt.pack(ctypes.c_float(referenceVoltagePositive_V).value)
        
        self.send_command_pdu(pdu)
        
        answer_pdu = self.receive_answer_pdu()
        self.__check_answer_pdu(answer_pdu, CommandCode.Calibration_Process, 0)
        
    def calibration_process_gain_negative(self,
                                          referenceVoltageNegative_V: float) -> None:
        """
        Process ADC gain calibration (negative measurement).
        
        :param referenceVoltageNegative_V:      Measured reference voltage (V), negative polarization (differential), range 1 mV to 1 kV.
        """
        pdu = host_protocol.PDU()
        pdu.header.pdu_type = host_protocol.PDU_Type.Command
        pdu.header.sequence_counter = self.host_protocol.get_command_sequence_counter()
        pdu.header.command = CommandCode.Calibration_Process
        pdu.header.status = 0
        pdu.payload = bytearray(1 + 4)
        
        pdu.payload[0] = CalibrationStage.Stage_3_Gain_Vin_Negative
        
        referenceVoltageNegative_V = float(referenceVoltageNegative_V)
        fmt = struct.Struct('<f')
        pdu.payload[1 : 1 + 4] = fmt.pack(ctypes.c_float(referenceVoltageNegative_V).value)
        
        self.send_command_pdu(pdu)
        
        answer_pdu = self.receive_answer_pdu()
        self.__check_answer_pdu(answer_pdu, CommandCode.Calibration_Process, 0)
    
    def calibration_data_read(self) -> calibration_container.CalibrationContainer:
        """
        Read calibration data from NVM.
        
        :return:     Calibration data.
        """
        pdu = host_protocol.PDU()
        pdu.header.pdu_type = host_protocol.PDU_Type.Command
        pdu.header.sequence_counter = self.host_protocol.get_command_sequence_counter()
        pdu.header.command = CommandCode.Calibration_Data_Read
        pdu.header.status = 0
        
        self.send_command_pdu(pdu)
        
        answer_pdu = self.receive_answer_pdu()
        payload_length = calibration_container.get_empty_container_size() + ADC_CALIBRATION_DATA_SIZE
        self.__check_answer_pdu(answer_pdu, CommandCode.Calibration_Data_Read, payload_length)
        
        _, device_identification = self.get_device_info()
        
        container = calibration_container.CalibrationContainer(hardware_type=device_identification.hardware_type,
                                                               hardware_variant=device_identification.hardware_variant,
                                                               device_data_size=ADC_CALIBRATION_DATA_SIZE)
        container.parse(answer_pdu.payload)
        
        return container
    
    def calibration_data_write(self, calibration_data: calibration_container.CalibrationContainer) -> None:
        """
        Write calibration data to NVM.
        
        :param calibration_data:    Calibration data.
        """
        if not isinstance(calibration_data, calibration_container.CalibrationContainer):
            raise ValueError("Invalid type")
        
        raw_data_size = calibration_container.get_empty_container_size() + ADC_CALIBRATION_DATA_SIZE
        if len(calibration_data.raw_data) != raw_data_size:
            raise ValueError("Invalid size")
        
        pdu = host_protocol.PDU()
        pdu.header.pdu_type = host_protocol.PDU_Type.Command
        pdu.header.sequence_counter = self.host_protocol.get_command_sequence_counter()
        pdu.header.command = CommandCode.Calibration_Data_Write
        pdu.header.status = 0
        pdu.payload = calibration_data.raw_data
        
        self.send_command_pdu(pdu)
        
        answer_pdu = self.receive_answer_pdu()
        self.__check_answer_pdu(answer_pdu, CommandCode.Calibration_Data_Write, 0)
    
    @staticmethod
    def pdu_to_samples(pdu: host_protocol.PDU,
                       gain_coefficients: tuple[float, ...]) -> numpy.ndarray:
        """
        Convert PDU with acquisition raw data to samples.
        
        :param pdu:                     PDU with acquisition raw data.
        :param gain_coefficients:       Gain coefficients required to scale samples.
        
        :return:
        ADC samples in Numpy 2D array:
          * axis 0 = rows (index is sample number).
          * axis 1 = columns (index is enabled channel number).
        
        Sample format is float32, unit is V.
        """
        sample_bytes = pdu.payload[daq.PDU_PAYLOAD_OFFSET_ADC_SAMPLES :]
        sample_count = len(sample_bytes) // ADC_SAMPLE_SIZE
        
        assert sample_count > 0
        assert (len(sample_bytes) % ADC_SAMPLE_SIZE) == 0
        assert (gain_coefficients is not None) and (len(gain_coefficients) > 0)
        
        adc_channel_enabled_count = len(gain_coefficients)
        
        assert (adc_channel_enabled_count > 0) and (adc_channel_enabled_count <= ADC_CHANNEL_COUNT)
        
        adc_conversion_count = sample_count // adc_channel_enabled_count
        
        assert adc_conversion_count > 0
        assert (sample_count % adc_channel_enabled_count) == 0
        
        samples = numpy.empty(shape=(adc_conversion_count, adc_channel_enabled_count), dtype='float32')
        byte_index = 0
        adc_conversion_index = 0
        adc_channel_enabled_index = 0
        
        for sample_index in range(sample_count):
            sample_raw = sample_bytes[byte_index : byte_index + ADC_SAMPLE_SIZE]
            sample_24bit = int.from_bytes(bytes=sample_raw, byteorder="little", signed=True)
            
            samples[adc_conversion_index, adc_channel_enabled_index] = numpy.float32(sample_24bit) * numpy.float32(gain_coefficients[adc_channel_enabled_index])
            
            adc_channel_enabled_index += 1
            if adc_channel_enabled_index == adc_channel_enabled_count:
                adc_channel_enabled_index = 0
                adc_conversion_index += 1
            
            byte_index = sample_index * ADC_SAMPLE_SIZE
        
        return samples
    
    type ValidStatusCodes = tuple[device_basic.StatusCode | StatusCode, ...]
    
    @staticmethod
    def __check_answer_pdu(answer_pdu: host_protocol.PDU,
                           command_code: int,
                           payload_length: int,
                           status_codes: (ValidStatusCodes | None) = None) -> None:
        """
        Check answer PDU.
        
        :param answer_pdu:      PDU from device.
        :param command_code:    Expected command code.
        :param status_codes:    Expected status codes.
        :param payload_length:  Expected payload length.
        """
        if answer_pdu is None:
            raise DeviceException("No response from device")
        
        if answer_pdu.header.command != command_code:
            raise DeviceException(f"Invalid command code in answer: {answer_pdu.header.command}")
    
        if len(answer_pdu.payload) != payload_length:
            raise DeviceException(f"Invalid payload length: {len(answer_pdu.payload)}")

        if status_codes is None:
            status_codes = (device_basic.StatusCode.Success,)
            
        if answer_pdu.header.status not in status_codes:
            raise DeviceException(f"Invalid status code: {answer_pdu.header.status}")
