"""
DAQ_M02 basic functionality.
"""

import enum
import ctypes
import struct

import numpy

from . import host_protocol
from . import device_basic
from . import daq
from . import calibration_container

ADC_CHANNEL_COUNT = 1
ADC_SAMPLE_SIZE = 2
ADC_GAIN_COEFFICIENT_SIZE = 4
ADC_CALIBRATION_DATA_SIZE = 24

ranges_mA = (5, 50, 500, 5000)

class CommandCode(enum.IntEnum):
    """
    Device specific commands (range 128-255).
    """
    Acquisition_Start = 128
    Acquisition_Stop = 129
    Acquisition_Get_Status = 130
    Acquisition_Push_Data = 131
    Calibration_Process = 132
    Calibration_Data_Read = 133
    Calibration_Data_Write = 134

class StatusCode(enum.IntEnum):    
    """
    Device specific codes (range 128-255).
    """
    Acquisition_Data_Begin = 128
    Acquisition_Data_Continue = 129
    Acquisition_Data_End = 130
    ADC_Calibration_Completed = 131
    ADC_Calibration_Invalid_Input_Current = 132

class SamplerState(enum.IntEnum):
    Idle = 0
    WaitForTrigger = 1
    Acquisition = 2
    Completed = 3
    Stop = 4
    
class CurrentRange(enum.IntEnum):
    Range_5_mA = 0
    Range_50_mA = 1
    Range_500_mA = 2
    Range_5_A = 3
    
class TriggerMode(enum.IntEnum):
    Disabled = 0
    EdgeRising = 1
    EdgeFalling = 2
    EdgeBoth = 3
    
class CalibrationStage(enum.IntEnum):
    Invalid = 0
    Stage_1_Offset = 1
    Stage_2_Gain_Current_Positive = 2
    Stage_3_Gain_Current_Negative = 3

class AcquisitionStatus:
    def __init__(self) -> None:
        self.calibration_is_valid = False
        self.acquisition_state = SamplerState.Idle
        self.trigger_input_state = False
        
        self.adc_sampling_frequency_real = 0
        self.adc_saturation_positive = 0.0
        self.adc_saturation_negative = 0.0
        
        self.counter_sample_acquisition = 0
        self.counter_sample_total = 0
        self.counter_buffer_overflow_acquisition = 0
        self.counter_buffer_overflow_total = 0
        self.counter_trigger = 0
    
    def from_bytes(self, data: bytearray) -> None:
        i = 0
        
        self.calibration_is_valid =         bool((data[i] >> 0) & 0x1)
        self.acquisition_state =    SamplerState((data[i] >> 1) & 0x7)
        self.trigger_input_state =          bool((data[i] >> 4) & 0x1)
        i += 1
        
        self.adc_sampling_frequency_real =         int.from_bytes(data[i : i + 4], byteorder='little', signed=False)
        i += 4
        fmt = struct.Struct('<f')
        self.adc_saturation_positive = fmt.unpack(data[i : i + 4])[0]
        i += 4
        self.adc_saturation_negative = fmt.unpack(data[i : i + 4])[0]
        i += 4
    
        self.counter_sample_acquisition =          int.from_bytes(data[i : i + 8], byteorder='little', signed=False)
        i += 8
        self.counter_sample_total =                int.from_bytes(data[i : i + 8], byteorder='little', signed=False)
        i += 8
        self.counter_buffer_overflow_acquisition = int.from_bytes(data[i : i + 8], byteorder='little', signed=False)
        i += 8
        self.counter_buffer_overflow_total =       int.from_bytes(data[i : i + 8], byteorder='little', signed=False)
        i += 8
        self.counter_trigger =                     int.from_bytes(data[i : i + 8], byteorder='little', signed=False)
        
    def to_string(self) -> str:
        s = "State: {}\nCalibration: {}\nTrigger input: {}\n".format(
            self.acquisition_state.name, int(self.calibration_is_valid), int(self.trigger_input_state))
        
        s += "ADC real sampling frequency: {} Hz\n".format(self.adc_sampling_frequency_real)
        
        s += "Saturation:\n  Positive: {}\n  Negative: {}\n".format(
            float(self.adc_saturation_positive), float(self.adc_saturation_negative))
        
        s += "Counters:\n"
        
        s += "  Sample (acquisition): {}\n  Sample (total): {}\n".format(
            self.counter_sample_acquisition, self.counter_sample_total)
        
        buffer_overflow_acquisition_percent = 100.0 * self.counter_buffer_overflow_acquisition / self.counter_sample_acquisition
        buffer_overflow_total_percent = 100.0 * self.counter_buffer_overflow_total / self.counter_sample_total
        
        s += "  Overflow (acquisition): {} / {:.1f} %\n  Overflow (total): {} / {:.1f} %\n".format(
            self.counter_buffer_overflow_acquisition, buffer_overflow_acquisition_percent,
            self.counter_buffer_overflow_total, buffer_overflow_total_percent)
        
        s += "  Trigger: {}".format(self.counter_trigger)
        
        return s
    
    @staticmethod
    def get_length() -> int:
        return (1 + 3*4 + 5*8)

class DeviceException(Exception):
    pass

class DAQ_M02(daq.DAQ):
    """
    DAQ_M02 Data Acquisition Module.\n
    This class is intended to inherit and define custom DAQ_M02 devices.
    """
    def __init__(self) -> None:
        super().__init__()
        
    def acquisition_start(self,
                          trigger_mode: TriggerMode,
                          adc_current_range: CurrentRange,
                          adc_sampling_frequency: int,
                          adc_sample_count_single_acquisition: int) -> tuple[float, ...]:
        """
        Start data acquisition.\n
        This function only trigger acquisition process in DAQ.\n
        Application must register callback to receive PDUs with raw data from DAQ:\n
        ``self.set_command_answer_callback(daq_m02.CommandCode.Acquisition_Push_Data, self.adc_data_callback)``\n
        In callback application must extract samples from PDU payload.\n
        Processing should use dedicated thread.\n
        Example code is in ``class DAQ_M02_Streamer``.
        
        :param trigger_mode:                            Set trigger input mode: 0=disabled, 1=rising edge, 2=falling edge, 3=both edges
        :param adc_current_range:                       Set current range: 1=5mA, 2=50mA, 3=500mA, 4=5A
        :param adc_sampling_frequency:                  Set sampling frequency, range: 1 Hz to 250 kHz
        :param adc_sample_count_single_acquisition:     Number of samples in single triggered acquisition, range: 1 to 2**64-1
        
        :return:        Gain coefficients (A/sample).
        """
        pdu = host_protocol.PDU()
        pdu.header.pdu_type = host_protocol.PDU_Type.Command
        pdu.header.sequence_counter = self.host_protocol.get_command_sequence_counter()
        pdu.header.command = CommandCode.Acquisition_Start
        pdu.header.status = 0
        pdu.payload = bytearray(4 + 8 + 1 + 1)
        
        trigger_mode = TriggerMode(trigger_mode)
        adc_current_range = CurrentRange(adc_current_range)
        adc_sampling_frequency = int(adc_sampling_frequency)
        adc_sample_count_single_acquisition = int(adc_sample_count_single_acquisition)
        
        if adc_sampling_frequency < 1:
            raise ValueError("Invalid sampling frequency")
        
        if adc_sample_count_single_acquisition < 1:
            raise ValueError("Invalid sample count (single)")
        
        pdu.payload[0 : 0 + 4] = adc_sampling_frequency.to_bytes(4, byteorder='little', signed=False)
        pdu.payload[4 : 4 + 8] = adc_sample_count_single_acquisition.to_bytes(8, byteorder='little', signed=False)
        pdu.payload[12] = trigger_mode
        pdu.payload[13] = adc_current_range
        
        self.send_command_pdu(pdu)
        
        answer_pdu = self.receive_answer_pdu()
        self.__check_answer_pdu(answer_pdu, CommandCode.Acquisition_Start, ADC_GAIN_COEFFICIENT_SIZE)
    
        gain_coefficients = struct.unpack('f' * ADC_CHANNEL_COUNT, answer_pdu.payload)
        
        return gain_coefficients
    
    def acquisition_stop(self) -> None:
        """
        Stop ADC acquisition.
        """
        pdu = host_protocol.PDU()
        pdu.header.pdu_type = host_protocol.PDU_Type.Command
        pdu.header.sequence_counter = self.host_protocol.get_command_sequence_counter()
        pdu.header.command = CommandCode.Acquisition_Stop
        pdu.header.status = 0
        
        self.send_command_pdu(pdu)
        
        answer_pdu = self.receive_answer_pdu()
        self.__check_answer_pdu(answer_pdu, CommandCode.Acquisition_Stop, 0)
    
    def acquisition_get_status(self) -> AcquisitionStatus:
        """
        Get acquisition status.
        
        :return:     Acquisition status.
        """
        pdu = host_protocol.PDU()
        pdu.header.pdu_type = host_protocol.PDU_Type.Command
        pdu.header.sequence_counter = self.host_protocol.get_command_sequence_counter()
        pdu.header.command = CommandCode.Acquisition_Get_Status
        pdu.header.status = 0
        
        self.send_command_pdu(pdu)
        
        answer_pdu = self.receive_answer_pdu()
        self.__check_answer_pdu(answer_pdu, CommandCode.Acquisition_Get_Status, AcquisitionStatus.get_length())
        
        acquisition_status = AcquisitionStatus()
        acquisition_status.from_bytes(answer_pdu.payload)
        
        return acquisition_status
    
    def calibration_process(self,
                            calibration_stage: CalibrationStage,
                            current_range: CurrentRange,
                            current_reference_A: float) -> int:
        """
        Process ADC calibration.
        
        :param calibration_stage:       Calibration stage number.
        :param current_range:           Selected current range (only for gain calibration).
        :param current_reference_A:     Reference test input current in A (only for gain calibration).
        
        :return:    Command answer code.
        """
        pdu = host_protocol.PDU()
        pdu.header.pdu_type = host_protocol.PDU_Type.Command
        pdu.header.sequence_counter = self.host_protocol.get_command_sequence_counter()
        pdu.header.command = CommandCode.Calibration_Process
        pdu.header.status = 0
        pdu.payload = bytearray(1)
        
        pdu.payload[0] = calibration_stage
        
        if calibration_stage == CalibrationStage.Stage_1_Offset:
            pass
        elif calibration_stage in (CalibrationStage.Stage_2_Gain_Current_Positive, CalibrationStage.Stage_3_Gain_Current_Negative):
            pdu.payload += current_range.to_bytes(1, byteorder='little', signed=False)
            
            current_reference_A = float(current_reference_A)
            fmt = struct.Struct('<f')
            pdu.payload += fmt.pack(ctypes.c_float(current_reference_A).value)
        else:
            raise ValueError("Invalid stage")
        
        self.send_command_pdu(pdu)
        
        answer_pdu = self.receive_answer_pdu()
        self.__check_answer_pdu(answer_pdu, CommandCode.Calibration_Process, 0,
                                status_codes=(device_basic.StatusCode.Success,
                                              StatusCode.ADC_Calibration_Completed,
                                              StatusCode.ADC_Calibration_Invalid_Input_Current))

        return int(answer_pdu.header.status)
    
    def calibration_data_read(self) -> calibration_container.CalibrationContainer:
        """
        Read calibration data from NVM.
        
        :return:     Calibration data.
        """
        pdu = host_protocol.PDU()
        pdu.header.pdu_type = host_protocol.PDU_Type.Command
        pdu.header.sequence_counter = self.host_protocol.get_command_sequence_counter()
        pdu.header.command = CommandCode.Calibration_Data_Read
        pdu.header.status = 0
        
        self.send_command_pdu(pdu)
        
        answer_pdu = self.receive_answer_pdu()
        payload_length = calibration_container.get_empty_container_size() + ADC_CALIBRATION_DATA_SIZE
        self.__check_answer_pdu(answer_pdu, CommandCode.Calibration_Data_Read, payload_length)
        
        _, device_identification = self.get_device_info()
        
        container = calibration_container.CalibrationContainer(hardware_type=device_identification.hardware_type,
                                                               hardware_variant=device_identification.hardware_variant,
                                                               device_data_size=ADC_CALIBRATION_DATA_SIZE)
        container.parse(answer_pdu.payload)
        
        return container
    
    def calibration_data_write(self, calibration_data: calibration_container.CalibrationContainer) -> None:
        """
        Write calibration data to NVM.
        
        :param calibration_data:    Calibration data.
        """
        if not isinstance(calibration_data, calibration_container.CalibrationContainer):
            raise ValueError("Invalid type")
        
        raw_data_size = calibration_container.get_empty_container_size() + ADC_CALIBRATION_DATA_SIZE
        if len(calibration_data.raw_data) != raw_data_size:
            raise ValueError("Invalid size")
        
        pdu = host_protocol.PDU()
        pdu.header.pdu_type = host_protocol.PDU_Type.Command
        pdu.header.sequence_counter = self.host_protocol.get_command_sequence_counter()
        pdu.header.command = CommandCode.Calibration_Data_Write
        pdu.header.status = 0
        pdu.payload = calibration_data.raw_data
        
        self.send_command_pdu(pdu)
        
        answer_pdu = self.receive_answer_pdu()
        self.__check_answer_pdu(answer_pdu, CommandCode.Calibration_Data_Write, 0)
    
    @staticmethod
    def pdu_to_samples(pdu: host_protocol.PDU,
                       gain_coefficients: tuple[float, ...]) -> numpy.ndarray:
        """
        Convert PDU with acquisition raw data to samples.
        
        :param pdu:                     PDU with acquisition raw data.
        :param gain_coefficients:       Gain coefficients required to scale samples.
        
        :return:
        ADC samples in Numpy 1D array:
          * axis 0 = rows (index is sample number).
        
        Sample format is float32, unit is A.
        """
        sample_bytes = pdu.payload[daq.PDU_PAYLOAD_OFFSET_ADC_SAMPLES :]
        sample_count = len(sample_bytes) // ADC_SAMPLE_SIZE
        
        assert sample_count > 0
        assert (len(sample_bytes) % ADC_SAMPLE_SIZE) == 0
        
        dtype = numpy.dtype(numpy.int16)
        dtype = dtype.newbyteorder('<')

        int16_array = numpy.frombuffer(sample_bytes, dtype=dtype)
        
        assert (gain_coefficients is not None) and (len(gain_coefficients) > 0)
        
        gain_coefficient = gain_coefficients[0]
        samples = int16_array.astype(numpy.float32) * numpy.float32(gain_coefficient)
        
        return samples
    
    type ValidStatusCodes = tuple[device_basic.StatusCode | StatusCode, ...]
    
    @staticmethod
    def __check_answer_pdu(answer_pdu: host_protocol.PDU,
                           command_code: int,
                           payload_length: int,
                           status_codes: (ValidStatusCodes | None) = None) -> None:
        """
        Check answer PDU.
        
        :param answer_pdu:      PDU from device.
        :param command_code:    Expected command code.
        :param status_codes:    Expected status codes.
        :param payload_length:  Expected payload length.
        """
        if answer_pdu is None:
            raise DeviceException("No response from device")
        
        if answer_pdu.header.command != command_code:
            raise DeviceException(f"Invalid command code in answer: {answer_pdu.header.command}")
    
        if len(answer_pdu.payload) != payload_length:
            raise DeviceException(f"Invalid payload length: {len(answer_pdu.payload)}")

        if status_codes is None:
            status_codes = (device_basic.StatusCode.Success,)
            
        if answer_pdu.header.status not in status_codes:
            raise DeviceException(f"Invalid status code: {answer_pdu.header.status}")

def current_to_range(current_mA: int) -> CurrentRange:
    """
    Convert current value to current range.
    
    :param current_mA:       Current value in mA.
    
    :return:    Current range.
    """
    return CurrentRange(ranges_mA.index(int(current_mA)))
