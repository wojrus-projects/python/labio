"""
DAQ_M02 data collector.
"""

import logging
import datetime
from typing import cast

import numpy
import h5py
import progressbar

from . import daq_m02
from . import daq_m02_streamer

class DAQ_M02_DataCollector(daq_m02_streamer.DAQ_M02_Streamer):
    """
    DAQ_M02 data collector.
    """
    def __init__(self) -> None:
        super().__init__()
        self.__clear()

    def __clear(self) -> None:
        """
        Clear object.
        """
        self.numpy_array: (numpy.ndarray | None) = None
        self.hdf5_file: (h5py.File | None) = None
        self.hdf5_dataset: (h5py.Dataset | None) = None
        
        self.adc_sample_count_total = 0
        self.adc_sample_counter = 0
        self.adc_data_sequence_counter = 0
        
        self.show_progress = False

    def read_data(self,
                  enable_output_numpy_array: bool,
                  enable_output_hdf5_file: bool,
                  hdf5_file_name: str,
                  
                  trigger_mode: daq_m02.TriggerMode,
                  adc_current_range: daq_m02.CurrentRange,
                  adc_sampling_frequency: int,
                  adc_sample_count_single_acquisition: int,
                  adc_sample_count_total: int,
                  
                  show_progress: bool = False) -> (numpy.ndarray | None):
        """
        Process continuous data acquisition.\n
        Data is stored in RAM in Numpy array and/or in HDF5 file.\n
        
        :param enable_output_numpy_array:               Enable output Numpy array
        :param enable_output_hdf5_file:                 Enable output HDF5 file
        :param hdf5_file_name:                          HDF5 file name
        
        :param trigger_mode:                            Set trigger input mode: 0=disabled, 1=rising edge, 2=falling edge, 3=both edges
        :param adc_current_range:                       Set current range: 1=5mA, 2=50mA, 3=500mA, 4=5A
        :param adc_sampling_frequency:                  Set sampling frequency, range: 1 Hz to 250 kHz
        :param adc_sample_count_single_acquisition:     Number of samples in single triggered acquisition, range: 1 to 2**64-1
        :param adc_sample_count_total:                  Number of samples in all triggered acquisitions, range: 1 to 2**64-1
        
        :param show_progress:                           Enable CLI progress bar
        
        :return:
        ADC samples in Numpy 1D array:
          * axis 0 = rows (index is sample number).
          
        Sample format is float32, unit is A.
        """
        self.__clear()
        
        enable_output_numpy_array = bool(enable_output_numpy_array)
        enable_output_hdf5_file = bool(enable_output_hdf5_file)
        
        if not (enable_output_numpy_array or enable_output_hdf5_file):
            raise ValueError("Enable output: numpy array or HDF5 file")
        
        trigger_mode = daq_m02.TriggerMode(trigger_mode)
        adc_current_range = daq_m02.CurrentRange(adc_current_range)
        adc_sampling_frequency = int(adc_sampling_frequency)
        adc_sample_count_single_acquisition = int(adc_sample_count_single_acquisition)
        adc_sample_count_total = int(adc_sample_count_total)
        
        if adc_sampling_frequency < 1:
            raise ValueError("Invalid sampling frequency")
        
        if adc_sample_count_single_acquisition < 1:
            raise ValueError("Invalid sample count (single)")
        
        if adc_sample_count_total < adc_sample_count_single_acquisition:
            raise ValueError("Invalid sample count (total)")
            
        if trigger_mode == daq_m02.TriggerMode.Disabled:
            adc_sample_count_total = adc_sample_count_single_acquisition

        self.adc_sample_count_total = adc_sample_count_total
        self.adc_sample_counter = 0
        self.adc_data_sequence_counter = 0
        
        self.show_progress = show_progress
        if self.show_progress:
            self.progress_bar = progressbar.ProgressBar(max_value=adc_sample_count_total)
            self.progress_bar.start()
        
        #
        # Create numpy array.
        #
        if enable_output_numpy_array:
            self.numpy_array = numpy.empty(shape=(adc_sample_count_total,), dtype=numpy.float32)
        
        #
        # Create HDF5 dataset file.
        #
        if enable_output_hdf5_file:
            CHUNK_SIZE = 50_000
            
            acquisition_time_s = float(adc_sample_count_single_acquisition) / float(adc_sampling_frequency)
            if acquisition_time_s >= 1.0 and adc_sample_count_single_acquisition >= CHUNK_SIZE:
                adc_sample_chunks = (CHUNK_SIZE,)
            else:
                adc_sample_chunks = None
            
            self.hdf5_file = h5py.File(hdf5_file_name, "w")
            hdf5_group = self.hdf5_file.create_group("DAQ_M02")
            self.hdf5_dataset = hdf5_group.create_dataset("Measurements",
                                                          shape=(adc_sample_count_total,),
                                                          dtype=numpy.float32, chunks=adc_sample_chunks, track_order=True,
                                                          compression="gzip", compression_opts=4)

            _, device_identification = self.get_device_info()

            range_A = daq_m02.ranges_mA[adc_current_range] / 1000.0

            self.hdf5_dataset = cast(h5py.Dataset, self.hdf5_dataset)
            self.hdf5_dataset.attrs["Date"] = datetime.datetime.now().isoformat()
            self.hdf5_dataset.attrs["Device name"] = "DAQ_M02"
            self.hdf5_dataset.attrs["Device S/N"] = numpy.uint32(device_identification.serial_number)
            self.hdf5_dataset.attrs["ADC channel count"] = numpy.uint32(daq_m02.ADC_CHANNEL_COUNT)
            self.hdf5_dataset.attrs["ADC sampling frequency"] = numpy.uint32(adc_sampling_frequency)
            self.hdf5_dataset.attrs["ADC current range"] = numpy.float32(range_A)
            
        #
        # Start measurement.
        #
        self.start_streaming(trigger_mode = trigger_mode,
                             adc_current_range = adc_current_range,
                             adc_sampling_frequency = adc_sampling_frequency,
                             adc_sample_count_single_acquisition = adc_sample_count_single_acquisition,
                             adc_sample_count_total = adc_sample_count_total,
                             callback_on_samples = self.__callback_on_samples)

        if self.show_progress:
            self.progress_bar.finish()

        #
        # Check DAQ buffer overflow.
        #
        acquisition_status = self.acquisition_get_status()
        
        if acquisition_status.counter_buffer_overflow_total > 0:
            logging.warning("ADC buffer overflow ({} samples lost, resize to {} samples)".format(
                acquisition_status.counter_buffer_overflow_total, self.adc_sample_counter))

        #
        # Remove empty rows at end numpy array or HDF5 file.
        #
        assert self.adc_sample_counter <= adc_sample_count_total
        
        adc_sample_count_lost = adc_sample_count_total - self.adc_sample_counter
        
        if adc_sample_count_lost > 0:
            if self.numpy_array is not None:
                self.numpy_array = numpy.delete(self.numpy_array,
                                                slice(self.adc_sample_counter, self.adc_sample_counter + adc_sample_count_lost),
                                                axis=0)
            if self.hdf5_dataset is not None:
                self.hdf5_dataset.resize(self.adc_sample_counter, axis=0)
                self.hdf5_dataset.flush()
        
        if self.hdf5_dataset is not None:
            self.hdf5_dataset.attrs["ADC sampling frequency"] = numpy.uint32(acquisition_status.adc_sampling_frequency_real)
            
        if self.hdf5_file is not None:
            self.hdf5_file.close()
        
        return self.numpy_array
    
    def __callback_on_samples(self, samples: numpy.ndarray, sequence_counter: int) -> None:
        """
        Callback: receive samples from DAQ.
        """
        # Check data sequence counter.
        
        sequence_counter_delta = numpy.uint16(sequence_counter) - numpy.uint16(self.adc_data_sequence_counter)
        self.adc_data_sequence_counter = sequence_counter
        
        if sequence_counter_delta > 1:
            logging.warning("Data streamer lost {} segment(s)".format(sequence_counter_delta - 1))
        
        # Get samples.
        
        assert self.adc_sample_counter <= self.adc_sample_count_total
        
        if self.adc_sample_counter == self.adc_sample_count_total:
            return
        
        sample_count = len(samples)
        
        insert_index = self.adc_sample_counter
        insert_count = sample_count
        
        self.adc_sample_counter += sample_count
        if self.adc_sample_counter > self.adc_sample_count_total:
            self.adc_sample_counter = self.adc_sample_count_total
        
        if insert_index >= self.adc_sample_count_total:
            return
        
        insert_available = self.adc_sample_count_total - insert_index
        
        if insert_count > insert_available:
            insert_count = insert_available
        
        if insert_count == 0:
            return
        
        # Insert samples to output numpy array or HDF5 file.
        
        if self.numpy_array is not None:
            self.numpy_array[insert_index : insert_index + insert_count] = samples[0 : insert_count]
        
        if self.hdf5_dataset is not None:
            self.hdf5_dataset[insert_index : insert_index + insert_count] = samples[0 : insert_count]
        
        if self.show_progress:
            self.progress_bar.increment(insert_count)
