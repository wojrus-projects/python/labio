"""
Device information data.
"""

import enum

class HardwareType(enum.IntEnum):
    """
    Hardware type codes.
    """
    Unknown = 0
    DAQ_M01 = 1
    DAQ_M02 = 2
    DAQ_M03 = 3
    VREF_M01 = 4
    RELAY_USB_M01 = 5

device_names = {
    HardwareType.Unknown:       "Unknown",
    HardwareType.DAQ_M01:       "DAQ_M01",
    HardwareType.DAQ_M02:       "DAQ_M02",
    HardwareType.DAQ_M03:       "DAQ_M03",
    HardwareType.VREF_M01:      "VREF_M01",
    HardwareType.RELAY_USB_M01: "RELAY_USB_M01"
}

def get_device_name(hardware_type: HardwareType) -> str:
    """
    Get device name.
    
    :param hardware_type:   Hardware type code.
    
    :return:    Device name or "" if not exist.
    """
    device_name = ""
    
    try:
        device_name = device_names[HardwareType(hardware_type)]
    except KeyError:
        pass
    
    return device_name

def get_device_type(device_name: str) -> HardwareType:
    """
    Get device type.
    
    :param device_name:   Device name.
    
    :return:   Hardware type code.
    """
    hardware_type = HardwareType.Unknown
    
    for hardware_type, hardware_name in device_names.items():
        if hardware_name == device_name:
            return hardware_type
    
    return HardwareType.Unknown

class SoftwareVersion:
    """
    Software version: major.minor.patch (variant)
    """
    Size = 4
    
    def __init__(self) -> None:
        self.major = 0
        self.minor = 0
        self.patch = 0
        self.variant = 0

    def from_bytes(self, data: bytearray) -> None:
        self.major = int(data[0])
        self.minor = int(data[1])
        self.patch = int(data[2])
        self.variant = int(data[3])
        
    def to_string(self) -> str:
        return "Version: {}.{}.{} (Variant: {})".format(self.major, self.minor, self.patch, self.variant)

class SoftwareDate:
    """
    Software date: year-month-day
    """
    Size = 4
    
    def __init__(self) -> None:
        self.year = 0
        self.month = 0
        self.day = 0
        
    def from_bytes(self, data: bytearray) -> None:
        self.year = int(data[0]) + int(data[1]) * 256
        self.month = int(data[2])
        self.day = int(data[3])
        
    def to_string(self) -> str:
        return "{:04}-{:02}-{:02}".format(self.year, self.month, self.day)
        
class SoftwareInfo:
    """
    Software info: version + date
    """
    Size = 8
    
    def __init__(self) -> None:
        self.version = SoftwareVersion()
        self.date = SoftwareDate()
        
    def from_bytes(self, data: bytearray) -> None:
        if len(data) < SoftwareInfo.Size:
            raise ValueError("Invalid data length")
        
        self.version.from_bytes(data[0 : 4])
        self.date.from_bytes(data[4 : 8])
        
    def to_string(self) -> str:
        return "{} / Date: {}".format(self.version.to_string(), self.date.to_string())

class DeviceIdentification:
    """
    Device identification data.
    """
    Size = 28
    SerialNumberSize = 4
    SerialNumberMin = 0
    SerialNumberMax = (1 << (SerialNumberSize * 8)) - 1
    
    def __init__(self) -> None:
        self.hardware_type = HardwareType.Unknown
        self.hardware_variant = 0
        self.serial_number = 0
        self.firmware = SoftwareInfo()
        self.flashloader = SoftwareInfo()

    def from_bytes(self, data: bytearray, index: int) -> None:
        if len(data) < DeviceIdentification.Size:
            raise ValueError("Invalid data length")
        
        if index >= len(data):
            raise ValueError("Invalid index")
        
        available_bytes = len(data) - index
        
        if available_bytes < DeviceIdentification.Size:
            raise ValueError("Invalid data length")
        
        self.hardware_type = HardwareType(int.from_bytes(bytes=data[index : index + 4], byteorder='little', signed=False))
        index += 4
        self.hardware_variant = int.from_bytes(bytes=data[index : index + 4], byteorder='little', signed=False)
        index += 4
        self.serial_number = int.from_bytes(bytes=data[index : index + 4], byteorder='little', signed=False)
        index += 4
        self.firmware.from_bytes(data[index : index + SoftwareInfo.Size])
        index += SoftwareInfo.Size
        self.flashloader.from_bytes(data[index : index + SoftwareInfo.Size])
        
    def to_string(self) -> str:
        return "Hardware Type: {} ({})\nHardware Variant: {}\nS/N: {}\nFirmware: {}\nFlashloader: {}".format(
            self.hardware_type, self.hardware_type.name,
            self.hardware_variant, self.serial_number,
            self.firmware.to_string(), self.flashloader.to_string())
    
    def serial_number_is_valid(self) -> bool:
        return (self.serial_number > DeviceIdentification.SerialNumberMin) \
            and (self.serial_number < DeviceIdentification.SerialNumberMax)
    
class Device_Status:
    """
    Device status data.
    """
    Size = 4

    def __init__(self) -> None:
        self.frame_length_max = 0
        self.flashloader_enabled = False

    def from_bytes(self, data: bytearray, index: int) -> None:
        if len(data) < Device_Status.Size:
            raise ValueError("Invalid data length")
        
        if index >= len(data):
            raise ValueError("Invalid index")
        
        available_bytes = len(data) - index
        
        if available_bytes < Device_Status.Size:
            raise ValueError("Invalid data length")
        
        self.frame_length_max = int.from_bytes(bytes=data[index : index + 2], byteorder='little', signed=False)
        index += 2
        self.flashloader_enabled = bool((data[index] >> 0) & 0x1)
        
    def to_string(self) -> str:
        return "Frame Length Max: {}\nFlashloader Enabled: {}".format(
            self.frame_length_max, int(self.flashloader_enabled))
