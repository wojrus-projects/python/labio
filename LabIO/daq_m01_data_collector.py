"""
DAQ_M01 data collector.
"""

import logging
import datetime
from typing import cast

import numpy
import h5py
import progressbar

from . import daq_m01
from . import daq_m01_streamer

class DAQ_M01_DataCollector(daq_m01_streamer.DAQ_M01_Streamer):
    """
    DAQ_M01 data collector.
    """
    def __init__(self) -> None:
        super().__init__()
        self.__clear()

    def __clear(self) -> None:
        """
        Clear object.
        """
        self.numpy_array: (numpy.ndarray | None) = None
        self.hdf5_file: (h5py.File | None) = None
        self.hdf5_dataset: (h5py.Dataset | None) = None
        
        self.adc_sample_count_max = 0
        self.adc_sample_counter = 0
        self.adc_data_sequence_counter = 0
        
        self.show_progress = False

    def read_data(self,
                  enable_output_numpy_array: bool,
                  enable_output_hdf5_file: bool,
                  hdf5_file_name: str,
                  
                  adc_sampling_frequency: int,
                  adc_sample_count_max: int,
                  adc_channel_state: list[int],
                  adc_pga_gain: list[int],
                  
                  show_progress: bool = False) -> (numpy.ndarray | None):
        """
        Process continuous data acquisition.\n
        Data is stored in RAM in Numpy array and/or in HDF5 file.\n
        
        :param enable_output_numpy_array:       Enable output Numpy array
        :param enable_output_hdf5_file:         Enable output HDF5 file
        :param hdf5_file_name:                  HDF5 file name
        
        :param adc_sampling_frequency:          Set sampling frequency, available: 250, 500, 1000, 2000, 4000, 8000, 16000, 32000 Hz
        :param adc_sample_count_max:            Number of samples in single acquisition, range: 1 to 2**64-1
        :param adc_channel_state:               ADC channels state: 0=disable, 1=enable
        :param adc_pga_gain:                    Set PGA gain for channels 1-6: 1, 2, 4, 8, 16, 32, 64, 128
        
        :param show_progress:                   Enable CLI progress bar
        
        :return:
        ADC samples in Numpy 2D array:
          * axis 0 = rows (index is sample number).
          * axis 1 = columns (index is enabled channel number).
        
        Sample format is float32, unit is V.
        """
        self.__clear()
        
        enable_output_numpy_array = bool(enable_output_numpy_array)
        enable_output_hdf5_file = bool(enable_output_hdf5_file)
        
        if not (enable_output_numpy_array or enable_output_hdf5_file):
            raise ValueError("Enable output: numpy array or HDF5 file")
        
        adc_sampling_frequency = int(adc_sampling_frequency)
        adc_sample_count_max = int(adc_sample_count_max)
        
        if adc_sampling_frequency < 250:
            raise ValueError("Invalid sampling frequency")
        
        if adc_sample_count_max < 1:
            raise ValueError("Invalid sample count")
        
        if (len(adc_channel_state) != daq_m01.ADC_CHANNEL_COUNT) or \
            (len(adc_pga_gain) != daq_m01.ADC_CHANNEL_COUNT):
            raise ValueError("Invalid length")
        
        adc_channel_count = 0
        
        for state in adc_channel_state:
            if int(state) != 0:
                adc_channel_count += 1
                
        if adc_channel_count == 0:
            raise ValueError("All channels are disabled")
        
        self.adc_sample_count_max = adc_sample_count_max
        self.adc_sample_counter = 0
        self.adc_data_sequence_counter = 0
        
        self.show_progress = show_progress
        if self.show_progress:
            self.progress_bar = progressbar.ProgressBar(max_value=self.adc_sample_count_max)
            self.progress_bar.start()
        
        #
        # Create numpy array.
        #
        if enable_output_numpy_array:
            shape = (self.adc_sample_count_max, adc_channel_count)
            self.numpy_array = numpy.empty(shape=shape, dtype=numpy.float32)
        
        #
        # Create HDF5 dataset file.
        #
        if enable_output_hdf5_file:
            CHUNK_SIZE = 1000
            
            acquisition_time_s = float(self.adc_sample_count_max) / float(adc_sampling_frequency)
            if acquisition_time_s >= 1.0 and self.adc_sample_count_max >= CHUNK_SIZE:
                adc_sample_chunks = (CHUNK_SIZE, adc_channel_count)
            else:
                adc_sample_chunks = None
            
            self.hdf5_file = h5py.File(hdf5_file_name, "w")
            hdf5_group = self.hdf5_file.create_group("DAQ_M01")
            self.hdf5_dataset = hdf5_group.create_dataset("Measurements",
                                                          shape=(self.adc_sample_count_max, adc_channel_count),
                                                          dtype=numpy.float32, chunks=adc_sample_chunks, track_order=True,
                                                          compression="gzip", compression_opts=4)

            _, device_identification = self.get_device_info()

            self.hdf5_dataset = cast(h5py.Dataset, self.hdf5_dataset)
            self.hdf5_dataset.attrs["Date"] = datetime.datetime.now().isoformat()
            self.hdf5_dataset.attrs["Device name"] = "DAQ_M01"
            self.hdf5_dataset.attrs["Device S/N"] = numpy.uint32(device_identification.serial_number)
            self.hdf5_dataset.attrs["ADC sampling frequency"] = numpy.uint32(adc_sampling_frequency)
            self.hdf5_dataset.attrs["ADC channel count total"] = numpy.uint32(daq_m01.ADC_CHANNEL_COUNT)
            self.hdf5_dataset.attrs["ADC channel count enabled"] = numpy.uint32(adc_channel_count)
            self.hdf5_dataset.attrs["ADC channel state"] = adc_channel_state
            self.hdf5_dataset.attrs["ADC channel gain"] = adc_pga_gain
            
        #
        # Start measurement.
        #
        self.start_streaming(adc_sampling_frequency = adc_sampling_frequency,
                             adc_sample_count_max = self.adc_sample_count_max,
                             adc_channel_state = adc_channel_state,
                             adc_pga_gain = adc_pga_gain,
                             callback_on_samples = self.__callback_on_samples)

        if self.show_progress:
            self.progress_bar.finish()

        #
        # Check DAQ buffer overflow.
        #
        acquisition_status = self.acquisition_get_status()
        
        if acquisition_status.counter_buffer_overflow > 0:
            logging.warning("ADC buffer overflow ({} samples lost, resize to {} samples)".format(
                acquisition_status.counter_buffer_overflow, self.adc_sample_counter))

        #
        # Remove empty rows at end numpy array or HDF5 file.
        #
        assert self.adc_sample_counter <= self.adc_sample_count_max
        
        adc_sample_count_lost = self.adc_sample_count_max - self.adc_sample_counter
        
        if adc_sample_count_lost > 0:
            if self.numpy_array is not None:
                self.numpy_array = numpy.delete(self.numpy_array,
                                                slice(self.adc_sample_counter, self.adc_sample_counter + adc_sample_count_lost),
                                                axis=0)
            if self.hdf5_dataset is not None:
                self.hdf5_dataset.resize(self.adc_sample_counter, axis=0)
                self.hdf5_dataset.flush()
        
        if self.hdf5_file is not None:
            self.hdf5_file.close()
        
        return self.numpy_array
    
    def __callback_on_samples(self, samples: numpy.ndarray, sequence_counter: int) -> None:
        """
        Callback: receive samples from DAQ.
        """
        # Check data sequence counter.
        
        sequence_counter_delta = numpy.uint16(sequence_counter) - numpy.uint16(self.adc_data_sequence_counter)
        self.adc_data_sequence_counter = sequence_counter
        
        if sequence_counter_delta > 1:
            logging.warning("Data streamer lost {} segment(s)".format(sequence_counter_delta - 1))
        
        # Get samples.
        
        assert self.adc_sample_counter <= self.adc_sample_count_max
        
        if self.adc_sample_counter == self.adc_sample_count_max:
            return
        
        sample_count = len(samples)
        
        insert_index = self.adc_sample_counter
        insert_count = sample_count
        
        self.adc_sample_counter += sample_count
        if self.adc_sample_counter > self.adc_sample_count_max:
            self.adc_sample_counter = self.adc_sample_count_max
        
        if insert_index >= self.adc_sample_count_max:
            return
        
        insert_available = self.adc_sample_count_max - insert_index
        
        if insert_count > insert_available:
            insert_count = insert_available
        
        if insert_count == 0:
            return
        
        # Insert samples to output numpy array or HDF5 file.
        
        if self.numpy_array is not None:
            self.numpy_array[insert_index : insert_index + insert_count] = samples[0 : insert_count]
        
        if self.hdf5_dataset is not None:
            self.hdf5_dataset[insert_index : insert_index + insert_count] = samples[0 : insert_count]
        
        if self.show_progress:
            self.progress_bar.increment(insert_count)
