"""
Utilities library.
"""

import serial.serialutil
import serial.tools.list_ports

from LabIO import device_basic
from LabIO import device_info

USB_VID = 0x0483    # = STMicroelectronics
USB_PID = 0x5740    # = Virtual COM Port

class DeviceProbeResult:
    """
    Device probing result data.
    """
    def __init__(self,
                 port_name: str,
                 device_status: device_info.Device_Status,
                 device_identification: device_info.DeviceIdentification) -> None:
        self.port_name = port_name
        self.device_status = device_status
        self.device_identification = device_identification

def probe_device(port_name: str) -> (DeviceProbeResult | None):
    """
    Probe device.
    
    :param port_name:   Port name.
    
    :return:   Probe result: port name, device status, device identification data. If no response then return None.
    """
    device = device_basic.DeviceBasic()
    probe_result = None
    
    try:
        device.connect(interface_name=port_name)
        device_status, device_identification = device.get_device_info()
        probe_result = DeviceProbeResult(port_name, device_status, device_identification)
    except (serial.serialutil.SerialException, device_basic.DeviceException):
        pass
    finally:
        device.disconnect()
        
    return probe_result

def find_devices() -> tuple[DeviceProbeResult, ...]:
    """
    Find connected USB devices compatible with LabIO.
    """
    ports = serial.tools.list_ports.comports()
    devices = []
    
    for port in ports:
        if (port.vid is not None) and (port.vid == USB_VID) and \
           (port.pid is not None) and (port.pid == USB_PID):
            probe_result = probe_device(port.device)
            if probe_result:
                devices.append(probe_result)

    return tuple(devices)
