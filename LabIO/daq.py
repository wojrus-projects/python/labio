"""
DAQ type devices basic functionality.
"""

from collections.abc import Callable

import numpy

from . import host_protocol
from . import device_basic_serial_number

ADC_DATA_SEQUENCE_COUNTER_SIZE = 2

# Host PDU payload layout with ADC acquisition data.
PDU_PAYLOAD_OFFSET_ADC_DATA_SEQUENCE_COUNTER = 0
PDU_PAYLOAD_OFFSET_ADC_SAMPLES = PDU_PAYLOAD_OFFSET_ADC_DATA_SEQUENCE_COUNTER + ADC_DATA_SEQUENCE_COUNTER_SIZE

type CallbackOnSamples = Callable[[numpy.ndarray, int], None]

class DAQ(device_basic_serial_number.DeviceBasicSerialNumber):
    """
    DAQ basic functionality.\n
    This class is intended to inherit and define custom DAQ devices.
    """
    def __init__(self) -> None:
        super().__init__()

    @staticmethod
    def get_adc_data_sequence_counter(pdu: host_protocol.PDU) -> int:
        """
        Get ADC data sequence counter from PDU.
        
        :param pdu:      PDU with acquisition raw data.
        
        :return:         ADC data sequence counter.
        """
        assert len(pdu.payload) >= ADC_DATA_SEQUENCE_COUNTER_SIZE
        
        offset = PDU_PAYLOAD_OFFSET_ADC_DATA_SEQUENCE_COUNTER
        sc_bytes = pdu.payload[offset : offset + ADC_DATA_SEQUENCE_COUNTER_SIZE]
        adc_data_sequence_counter = int.from_bytes(sc_bytes, byteorder='little', signed=False)
        
        return adc_data_sequence_counter
