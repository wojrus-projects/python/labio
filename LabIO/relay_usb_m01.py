"""
RELAY_USB_M01 basic functionality.
"""

from . import device_basic_serial_number

RELAY_SWITCH_TIME_ms = 5

class RELAY_USB_M01(device_basic_serial_number.DeviceBasicSerialNumber):
    """
    RELAY_USB_M01 Relay Module.
    """
    def __init__(self) -> None:
        super().__init__()
    
    def set_relay(self, state_bitmap: int) -> None:
        """
        Set relays.
        
        :param state_bitmap:    Bitmap (8 bits) with relays states. Bit 0 = relay #1, bit 7 = relay #8.
        """
        state_bitmap = int(state_bitmap) & 0xFF
        
        self.digital_io_write(state_bitmap)
