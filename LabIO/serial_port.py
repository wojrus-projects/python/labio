"""
Serial port library (optimized for USB CDC Virtual Serial Port).
"""

import logging
import threading
import queue
import platform

import serial

class SerialPort:
    """
    Serial port with Rx FIFO buffer.
    """
    def __init__(self) -> None:
        self.uart = serial.Serial()
        self.uart.timeout = 0.001
        self.uart.write_timeout = 1.0
        
        system = platform.system()
        if system == 'Windows':
            self.uart.set_buffer_size(rx_size=64*1024, tx_size=64*1024)
        
        self.is_open = False
        self.port_name = ""
        
        self.rx_fifo: queue.Queue = queue.Queue(maxsize=0)
        self.thread_receive_event = threading.Event()
    
    def open(self, port_name: str) -> None:
        """
        Open port (non thread safe).
        
        :param port_name:   Port name.
        
        :raises serial.serialutil.SerialException:   If serial port error occured.
        """
        if not self.is_open:
            logging.info(f"Serial port: open {port_name}")
            
            # Clear Rx FIFO.
            self.rx_fifo = queue.Queue(maxsize=0)
            
            # Open port.
            self.uart.port = port_name
            self.uart.open()
            self.uart.reset_input_buffer()
            self.uart.reset_output_buffer()
            
            # Start Rx thread.
            self.thread_receive_event = threading.Event()
            self.thread_receive = threading.Thread(target=self.__receive_worker, name='Serial port receiver', daemon=True)
            self.thread_receive.start()
            
            self.port_name = port_name
            self.is_open = True
    
    def close(self) -> None:
        """
        Close port (non thread safe).
        """
        if self.is_open:
            logging.info(f"Serial port ({self.port_name}): close")
            
            # Stop Rx thread.
            self.thread_receive_event.set()
            self.thread_receive.join(timeout=0.5)
            
            # Close port.
            try:
                self.uart.close()
            except Exception:
                pass
            
            self.port_name = ""
            self.is_open = False
            
    def __terminate(self, wait_for_thread: bool = False) -> None:
        """
        Shut down port if exception occured.
        """
        if self.is_open:
            logging.warning(f"Serial port ({self.port_name}): shut down")
            
            # Begin terminate Rx thread.
            self.thread_receive_event.set()
            
            if wait_for_thread:
                self.thread_receive.join(timeout=0.5)
            
            # Close port.
            try:
                self.uart.close()
            except Exception:
                pass
            
            self.port_name = ""
            self.is_open = False

    def read(self, timeout_s: float = 0.1) -> bytearray:
        """
        Get Rx data from FIFO buffer (blocking mode, thread safe).
        
        :param timeout:     Read timeout in seconds.
        
        :return:    Serial port Rx data. If timeout occured then return None.
        """
        try:
            rx_data = self.rx_fifo.get(block=True, timeout=timeout_s)
        except queue.Empty:
            rx_data = None
            
        return rx_data
    
    def write(self, tx_data: bytearray) -> bool:
        """
        Write data to port (blocking mode, thread safe).
        
        :param tx_data:     Data to send.
        
        :return:    Write result. If serial port is closed then return False.
        """
        is_write_success = False
        
        if self.is_open:
            try:
                self.uart.write(tx_data)
                is_write_success = True
            except serial.serialutil.SerialException:
                logging.error(f"Serial port ({self.port_name}): write exception")
                self.__terminate(wait_for_thread=True)
                
        return is_write_success
    
    def __receive_worker(self) -> None:
        """
        Serial port Rx thread worker.
        """
        logging.info(f"Serial port ({self.port_name}): Rx worker start")
        
        while not self.thread_receive_event.is_set():
            try:
                rx_data = self.uart.read(size=64*1024)
                if rx_data:
                    self.rx_fifo.put(item=bytearray(rx_data), block=True, timeout=None)
            except serial.serialutil.SerialException:
                logging.error(f"Serial port ({self.port_name}): read exception")
                self.__terminate()
        
        logging.info(f"Serial port ({self.port_name}): Rx worker stop")
