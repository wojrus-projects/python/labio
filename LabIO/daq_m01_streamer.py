"""
DAQ_M01 streamer.
"""

import threading
import queue
import logging
import time

from . import daq
from . import daq_m01
from . import host_protocol

class DAQ_M01_Streamer(daq_m01.DAQ_M01):
    """
    DAQ_M01 streamer.\n
    This class is intended to inherit and define custom DAQ_M01 streaming devices.
    """
    def __init__(self) -> None:
        super().__init__()
        self.__clear()
        
        self.set_command_answer_callback(daq_m01.CommandCode.Acquisition_Push_Data,
                                         self.__command_answer_callback)
    def __clear(self) -> None:
        """
        Clear object.
        """
        self.__host_protocol_fifo: queue.Queue = queue.Queue(maxsize=0)
        self.__host_protocol_exception: (host_protocol.HostProtocolException | None) = None
        
        self.__callback_on_samples: (daq.CallbackOnSamples | None) = None
        self.__thread_worker_is_enabled = False
        
        self.__acquisition_is_complete = False
        self.__sample_counter = 0
        
        self.__gain_coefficients: tuple[float, ...] = ()

    def start_streaming(self,
                        adc_sampling_frequency: int,
                        adc_sample_count_max: int,
                        adc_channel_state: list[int],
                        adc_pga_gain: list[int],
                        callback_on_samples: daq.CallbackOnSamples) -> None:
        """
        Process continuous data acquisition.\n
        Received samples are pushed to application via callback.
        
        :param adc_sampling_frequency:    Set sampling frequency, available: 250, 500, 1000, 2000, 4000, 8000, 16000, 32000 Hz
        :param adc_sample_count_max:      Number of samples in single acquisition, range: 1 to 2**64-1
        :param adc_channel_state:         ADC channels state: 0=disable, 1=enable
        :param adc_pga_gain:              Set PGA gain for channels 1-6: 1, 2, 4, 8, 16, 32, 64, 128
        :param callback_on_samples:       Callback on received samples (called from private thread).\n
        Callback parameters:\n
          * ADC samples (numpy.ndarray)
          * ADC data sequence counter (int)
        """
        self.__clear()
        
        adc_sampling_frequency = int(adc_sampling_frequency)
        adc_sample_count_max = int(adc_sample_count_max)
        
        if adc_sampling_frequency < 250:
            raise ValueError("Invalid sampling frequency")
        
        if adc_sample_count_max < 1:
            raise ValueError("Invalid sample count")
        
        if (len(adc_channel_state) != daq_m01.ADC_CHANNEL_COUNT) or \
            (len(adc_pga_gain) != daq_m01.ADC_CHANNEL_COUNT):
            raise ValueError("Invalid length")
        
        adc_channel_count = 0
        
        for state in adc_channel_state:
            if int(state) != 0:
                adc_channel_count += 1
                
        if adc_channel_count == 0:
            raise ValueError("All channels are disabled")
        
        #
        # Check calibration.
        #
        acquisition_status = self.acquisition_get_status()
        
        if not acquisition_status.calibration_is_valid:
            logging.warning("ADC calibration is invalid")
        
        #
        # Start measurement.
        #
        self.__sample_counter = 0
        self.__callback_on_samples = callback_on_samples
        self.__host_protocol_exception = None
        self.__acquisition_is_complete = False

        self.__gain_coefficients = self.acquisition_start(adc_sampling_frequency,
                                                          adc_sample_count_max,
                                                          adc_channel_state,
                                                          adc_pga_gain)

        thread = threading.Thread(target=self.__adc_data_worker, name='ADC data worker', daemon=True)
        thread.start()

        #
        # Wait for acquisition completion or host protocol exception.
        #
        while (not self.__acquisition_is_complete) and self.__thread_worker_is_enabled:
            time.sleep(0.1)

        #
        # Protocol return unrecovable error.
        # At this point USB serial port is disabled.
        #
        if self.__host_protocol_exception:
            raise self.__host_protocol_exception

        #
        # Stop acquisition.
        #
        self.acquisition_stop()

        #
        # Check DAQ buffer overflow.
        #
        acquisition_status = self.acquisition_get_status()
        
        if not acquisition_status.calibration_is_valid:
            logging.warning("ADC calibration is invalid")
        
        assert self.__sample_counter == (adc_sample_count_max - acquisition_status.counter_buffer_overflow)
        
        if acquisition_status.counter_buffer_overflow > 0:
            logging.warning("ADC buffer overflow ({} samples lost, resize to {} samples)".format(
                acquisition_status.counter_buffer_overflow, self.__sample_counter))
    
    def __command_answer_callback(self, protocol_object: host_protocol.PDU | host_protocol.HostProtocolException) -> None:
        """
        PDU/exception FIFO producer.\n
        This function is executed in host protocol PDU parser thread.
        
        :param protocol_object:     Answer PDU from device or host protocol exception.
        """
        # If protocol exception then clear application Rx FIFO.
        if isinstance(protocol_object, host_protocol.HostProtocolException):
            with self.__host_protocol_fifo.mutex:
                self.__host_protocol_fifo.queue.clear()
        
        self.__host_protocol_fifo.put(protocol_object, block=True, timeout=None)

    def __adc_data_worker(self) -> None:
        """
        PDU/exception FIFO consumer and data worker.
        """
        self.__thread_worker_is_enabled = True
        
        logging.info("ADC data worker start")
        
        while not self.__acquisition_is_complete:
            try:
                # Get PDU with ADC data or host protocol exception.
                
                queue_item = self.__host_protocol_fifo.get(block=True, timeout=0.1)
                
                if isinstance(queue_item, host_protocol.PDU):
                    
                    # Process ADC data from PDU.
                    
                    pdu = queue_item
                    samples = self.pdu_to_samples(pdu, self.__gain_coefficients)
                    sequence_counter = self.get_adc_data_sequence_counter(pdu)
                    self.__sample_counter += len(samples)
                    if self.__callback_on_samples:
                        self.__callback_on_samples(samples, sequence_counter)
                    
                    if pdu.header.status == daq_m01.StatusCode.Acquisition_Data_End:
                        self.__acquisition_is_complete = True
                        
                elif isinstance(queue_item, host_protocol.HostProtocolException):
                    
                    # On communication error save error and gracefully finish ADC data thread.
                    
                    self.__host_protocol_exception = queue_item
                    self.__acquisition_is_complete = True
                else:
                    assert False

            except queue.Empty:
                pass
                
        logging.info("ADC data worker stop")

        self.__thread_worker_is_enabled = False
