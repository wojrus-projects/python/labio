"""
Device with minimal functionality (only standard commands).
"""

import threading
import queue
import logging
import enum
from collections.abc import Callable
from typing import cast

from . import host_protocol
from . import serial_port
from . import device_info

type CallbackOnHostProtocol = Callable[[host_protocol.PDU | host_protocol.HostProtocolException], None]

class CommandCode(enum.IntEnum):
    """
    Standard commands (range 0-127).
    """
    # Basic commands.
    
    Get_Device_Info = 0
    Reset = 1
    
    # Memory operations commands.
    
    Memory_Write = 16
    Memory_Read = 17
    Memory_Erase = 18
    Memory_Program = 19
    
    # Digital inputs/outputs commands.
    
    Digital_IO_Write = Memory_Write + 8

class StatusCode(enum.IntEnum):
    """
    Standard codes (range 0-127).
    """
    Success = 0
    Failure = 1
    Invalid_Command = 2
    Invalid_Argument = 3
    Invalid_Format = 4
    Invalid_Version = 5
    Invalid_Configuration = 6
    Invalid_Calibration = 7
    Invalid_Checksum = 8
    Invalid_State = 9
    Invalid_Memory_Type = 10
    Invalid_Memory_Offset = 11
    Invalid_Memory_Address = 12
    Invalid_Memory_Size = 13
    Invalid_Memory_Alignment = 14
    Buffer_Overflow = 15
    Out_Of_Memory = 16
    Timeout = 17
    Busy = 18
    No_Operation = 19
    Hardware_Failure = 20
    Connection_Failure = 21
    Memory_Write_Error = 22
    Memory_Read_Error = 23
    Memory_Erase_Error = 24
    Memory_Program_Error = 25

class DeviceResetMode(enum.IntEnum):
    Standard = 0
    Flashloader = 1

class MemoryID(enum.IntEnum):
    Configuration_Factory = 0
    Configuration_Firmware = 1

class DeviceException(Exception):
    pass

class DeviceBasic:
    """
    Device with minimal functionality.\n
    This class is intended to inherit and define custom devices.
    """
    def __init__(self) -> None:
        self.host_protocol = host_protocol.HostProtocol(self.__protocol_callback_success,
                                                        self.__protocol_callback_error)
        
        self.__is_connected = False
        self.__interface = serial_port.SerialPort()
        
        self.__thread_parser: (threading.Thread | None) = None
        self.__thread_parser_event = threading.Event()
        
        self.__command_answer_queue: queue.Queue = queue.Queue(maxsize=0)
        self.__command_answer_callback_command_code: (int | None) = None
        self.__command_answer_callback_handler: (CallbackOnHostProtocol | None) = None
        
    def set_command_answer_callback(self,
                                    command_code: int,
                                    callback: CallbackOnHostProtocol) -> None:
        """
        Set custom handler for:\n
        - Answer PDU based on command code in PDU.\n
        - Protocol exception.\n
        Handler is executed in PDU parser thread context.\n
        Use this function to process asynchronous answer PDUs or to catch protocol exceptions.
        
        Parameters:
        - command_code :    Single command code for filtering device answer PDU.
        - callback :        Callback function with arguments: device answer PDU or protocol exception.
        """
        self.__command_answer_callback_command_code = command_code
        self.__command_answer_callback_handler = callback
        
    def connect(self, interface_name: str) -> None:
        """
        Open serial port.\n
        Function can throw `serial.serialutil.SerialException` if serial port error occured.
        
        Parameters:
        - interface_name :      Interface name.
        """
        if not self.__is_connected:
            logging.info(f"Connect to {interface_name}")
            
            self.host_protocol.reset()
            self.__command_answer_queue = queue.Queue(maxsize=0)
            
            self.__interface.open(interface_name)
            self.__is_connected = True
            
            self.__thread_parser_event = threading.Event()
            self.__thread_parser = threading.Thread(target=self.__protocol_parser_worker,
                                                    name='Host protocol PDU parser',
                                                    daemon=True)
            self.__thread_parser.start()
    
    def disconnect(self) -> None:
        """
        Close serial port.
        """
        if self.__is_connected:
            logging.info("Disconnect")
            
            self.__thread_parser_event.set()
            self.__thread_parser = cast(threading.Thread, self.__thread_parser)
            self.__thread_parser.join(timeout=0.5)
            
            self.__interface.close()
            self.__is_connected = False
            
            self.__command_answer_queue = queue.Queue(maxsize=0)
            self.host_protocol.reset()

    def reset(self, reset_mode: DeviceResetMode) -> None:
        """
        Reset device.
        
        After call this function application must:
        - Immediately close serial port.
        - Wait minimum 5 seconds for device reboot and USB enumeration.
        - Reopen serial port.
        
        Parameters:
        - reset_mode :   Device reset mode.
        """
        pdu = host_protocol.PDU()
        pdu.header.pdu_type = host_protocol.PDU_Type.Command
        pdu.header.sequence_counter = self.host_protocol.get_command_sequence_counter()
        pdu.header.command = CommandCode.Reset
        pdu.header.status = 0
    
        pdu.payload = bytearray(1)
        pdu.payload[0] = reset_mode
    
        self.send_command_pdu(pdu)
        
        answer_pdu = self.receive_answer_pdu()
        self.__check_answer_pdu(answer_pdu, CommandCode.Reset, 0)
    
    type DeviceInfo = tuple[device_info.Device_Status, device_info.DeviceIdentification]
    
    def get_device_info(self) -> DeviceInfo:
        """
        Get device status and HW/FW version.
        
        Return:     Device status and identification data.
        """
        pdu = host_protocol.PDU()
        pdu.header.pdu_type = host_protocol.PDU_Type.Command
        pdu.header.sequence_counter = self.host_protocol.get_command_sequence_counter()
        pdu.header.command = CommandCode.Get_Device_Info
        pdu.header.status = 0
        
        self.send_command_pdu(pdu)
        
        answer_pdu = self.receive_answer_pdu()
        self.__check_answer_pdu(answer_pdu, CommandCode.Get_Device_Info,
                                device_info.Device_Status.Size + device_info.DeviceIdentification.Size)

        device_status = device_info.Device_Status()
        device_status.from_bytes(answer_pdu.payload, 0)
        
        device_identification = device_info.DeviceIdentification()
        device_identification.from_bytes(answer_pdu.payload, device_info.Device_Status.Size)
        
        return (device_status, device_identification)
    
    def memory_write(self,
                     memory_id: MemoryID,
                     write_offset: int,
                     write_data: bytearray) -> int:
        """
        Write device memory.
        
        Parameters:
        - memory_id :       Memory index or type: 0 to 255.
        - write_offset :    Memory write offset in bytes: 0 to memory size - 1.
        - write_data :      Data to write, size: 0 to command PDU payload max. - 5 or memory size limit.
        
        Return:             Number of bytes written to memory.
        """
        pdu = host_protocol.PDU()
        pdu.header.pdu_type = host_protocol.PDU_Type.Command
        pdu.header.sequence_counter = self.host_protocol.get_command_sequence_counter()
        pdu.header.command = CommandCode.Memory_Write
        pdu.header.status = 0
        pdu.payload = bytearray(1 + 4)
        
        index = 0
        pdu.payload[index] = memory_id
        index += 1
        pdu.payload[index : index + 4] = write_offset.to_bytes(4, byteorder='little', signed=False)
        
        write_data_size_max = host_protocol.PAYLOAD_SIZE_MAX - (1 + 4)
        
        if len(write_data) > write_data_size_max:
            pdu.payload += write_data[0 : write_data_size_max]
        else:
            pdu.payload += write_data
        
        self.send_command_pdu(pdu)
        
        answer_pdu = self.receive_answer_pdu()
        self.__check_answer_pdu(answer_pdu, CommandCode.Memory_Write, 1 + 4 + 2)
        
        memory_id_answer = int.from_bytes(answer_pdu.payload[0 : 1], byteorder='little', signed=False)
        write_offset_answer = int.from_bytes(answer_pdu.payload[1 : 1 + 4], byteorder='little', signed=False)
        write_size = int.from_bytes(answer_pdu.payload[1 + 4 : 1 + 4 + 2], byteorder='little', signed=False)
        
        if memory_id_answer != memory_id:
            raise ValueError("Memory ID mismatch")
        
        if write_offset_answer != write_offset:
            raise ValueError("Write offset mismatch")
        
        return write_size
    
    def memory_read(self,
                    memory_id: MemoryID,
                    read_offset: int,
                    read_size: int) -> bytearray:
        """
        Read device memory.
        
        Parameters:
        - memory_id :    Memory index or type: 0 to 255.
        - read_offset :  Memory read offset in bytes: 0 to memory size - 1.
        - read_size :    Read size in bytes: 0 to answer PDU payload max. - 5 or memory size limit.
        
        Return:          Data from device memory.
        """
        pdu = host_protocol.PDU()
        pdu.header.pdu_type = host_protocol.PDU_Type.Command
        pdu.header.sequence_counter = self.host_protocol.get_command_sequence_counter()
        pdu.header.command = CommandCode.Memory_Read
        pdu.header.status = 0
        pdu.payload = bytearray(1 + 4 + 2)
        
        index = 0
        pdu.payload[index] = memory_id
        index += 1
        pdu.payload[index : index + 4] = read_offset.to_bytes(4, byteorder='little', signed=False)
        index += 4
        pdu.payload[index : index + 2] = read_size.to_bytes(2, byteorder='little', signed=False)
        
        self.send_command_pdu(pdu)
        
        answer_pdu = self.receive_answer_pdu()
        self.__check_answer_pdu(answer_pdu, CommandCode.Memory_Read)

        if len(answer_pdu.payload) < (1 + 4):
            raise ValueError("Invalid length")

        memory_id_answer = int.from_bytes(answer_pdu.payload[0 : 1], byteorder='little', signed=False)
        read_offset_answer = int.from_bytes(answer_pdu.payload[1 : 1 + 4], byteorder='little', signed=False)
        
        if memory_id_answer != memory_id:
            raise ValueError("Memory ID mismatch")
        
        if read_offset_answer != read_offset:
            raise ValueError("Read offset mismatch")
        
        read_data = answer_pdu.payload[1 + 4 : ]

        return read_data
    
    def memory_erase(self,
                     memory_id: MemoryID,
                     erase_offset: int,
                     erase_size: int) -> None:
        """
        Erase device memory.
        
        Parameters:
        - memory_id :    Memory index or type: 0 to 255.
        - erase_offset : Memory erase offset in bytes: 0 to memory size - 1.
        - erase_size :   Erase size in bytes: 0 to memory size.
        """
        pdu = host_protocol.PDU()
        pdu.header.pdu_type = host_protocol.PDU_Type.Command
        pdu.header.sequence_counter = self.host_protocol.get_command_sequence_counter()
        pdu.header.command = CommandCode.Memory_Erase
        pdu.header.status = 0
        pdu.payload = bytearray(1 + 4 + 4)
        
        index = 0
        pdu.payload[index] = memory_id
        index += 1
        pdu.payload[index : index + 4] = erase_offset.to_bytes(4, byteorder='little', signed=False)
        index += 4
        pdu.payload[index : index + 4] = erase_size.to_bytes(4, byteorder='little', signed=False)
        
        self.send_command_pdu(pdu)
        
        answer_pdu = self.receive_answer_pdu()
        self.__check_answer_pdu(answer_pdu, CommandCode.Memory_Erase)
    
    def digital_io_write(self, state_bitmap: int) -> None:
        """
        Write digital outputs.
        
        Parameters:
        - state_bitmap :    Bitmap (64 bits) with outputs states. Bit 0 = output #1, bit 63 = output #64.
        """
        pdu = host_protocol.PDU()
        pdu.header.pdu_type = host_protocol.PDU_Type.Command
        pdu.header.sequence_counter = self.host_protocol.get_command_sequence_counter()
        pdu.header.command = CommandCode.Digital_IO_Write
        pdu.header.status = 0
        
        state_bitmap = int(state_bitmap) & 0xFFFF_FFFF_FFFF_FFFF
        pdu.payload = bytearray(state_bitmap.to_bytes(8, byteorder='little', signed=False))
        
        self.send_command_pdu(pdu)
        
        answer_pdu = self.receive_answer_pdu()
        self.__check_answer_pdu(answer_pdu, CommandCode.Digital_IO_Write)

    def send_command_pdu(self, pdu: host_protocol.PDU) -> bool:
        """
        Send command PDU to device.
        
        Notes:
        - If function returns False then application must check serial port state.
        - If serial port is closed then application must call `disconnect()` and then `connect()`.
        
        Parameters:
        - pdu :     PDU to send.
        
        Return:     Send result.
        """
        return self.__interface.write(pdu.to_bytes())

    def receive_answer_pdu(self, timeout_s: float = 2.0) -> host_protocol.PDU:
        """
        Receive PDU from device.
        
        Notes:
        - If function returns None (= timeout) then application must check serial port state.
        - If serial port is closed then application must call `disconnect()` and then `connect()`.
        - Function can throws `HostProtocolException` if answer from device is invalid.
        
        Parameters:
        - timeout_s :       Receive timeout in seconds.
        
        Return:             Answer PDU from device. If occured answer timeout then raise DeviceException.
        """
        answer_pdu = host_protocol.PDU()
        
        try:
            queue_item = self.__command_answer_queue.get(block=True, timeout=timeout_s)
            if isinstance(queue_item, host_protocol.PDU):
                answer_pdu = queue_item
            elif isinstance(queue_item, host_protocol.HostProtocolException):
                raise queue_item
            else:
                assert False
        except queue.Empty:
            raise DeviceException("Device answer timeout")
        
        return answer_pdu

    @staticmethod
    def __check_answer_pdu(answer_pdu: host_protocol.PDU,
                           command_code: CommandCode,
                           payload_length: int | None = None) -> None:
        """
        Check answer PDU.
        
        Parameters:
        - answer_pdu :      PDU from device.
        - command_code :    Expected command code.
        - payload_length :  Expected payload length.
        """
        if answer_pdu is None:
            raise DeviceException("No response from device")
        
        if answer_pdu.header.command != command_code:
            raise DeviceException(f"Invalid command code in answer: {answer_pdu.header.command}")
        
        if answer_pdu.header.status != StatusCode.Success:
            raise DeviceException(f"Invalid status code: {answer_pdu.header.status}")
    
        if payload_length is not None:
            if len(answer_pdu.payload) != payload_length:
                raise DeviceException(f"Invalid payload length: {len(answer_pdu.payload)}")

    def __protocol_callback_success(self, answer_pdu: host_protocol.PDU) -> None:
        """
        Callback on valid device answer PDU.\n
        Function can filter PDUs by command code (use set_command_answer_callback).
        
        Parameters:
        - answer_pdu :      PDU from device
        """
        if (self.__command_answer_callback_command_code is not None) and \
            (answer_pdu.header.command == self.__command_answer_callback_command_code):
            if self.__command_answer_callback_handler:
                self.__command_answer_callback_handler(answer_pdu)
        else:
            self.__command_answer_queue.put(answer_pdu, block=True, timeout=None)

    def __protocol_callback_error(self, exception: host_protocol.HostProtocolException) -> None:
        """
        Callback on protocol parsing errors.\n
        They are always unrecovable.
        
        Parameters:
        - exception :       Host protocol exception.
        """
        #
        # On error gracefully finish protocol thread.
        #
        self.__interface.close()
        self.__thread_parser_event.set()
        
        if self.__command_answer_callback_command_code is not None:
            if self.__command_answer_callback_handler:
                self.__command_answer_callback_handler(exception)
        else:
            self.__command_answer_queue.put(exception, block=True, timeout=None)

    def __protocol_parser_worker(self) -> None:
        """
        Protocol parser worker.
        """
        logging.info("Protocol parser worker start")
        
        while (not self.__thread_parser_event.is_set()):
            rx_data = self.__interface.read(timeout_s=0.001)
            if rx_data:
                self.host_protocol.parse(rx_data)

        logging.info("Protocol parser worker stop")
