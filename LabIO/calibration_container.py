"""
Calibration data container.
"""

import enum

from . import crc16_modbus
from . import device_info

class ContainerState(enum.IntEnum):
    Invalid = 0
    Valid = 1

class ContainerHeader:
    """
    Calibration container header.
    """
    size = 4 + 3*4 + 1 + 16
    
    def __init__(self,
                 hardware_type: device_info.HardwareType,
                 hardware_variant: int) -> None:
        """
        Constructor.
        
        :param hardware_type:       Expected hardware type.
        :param hardware_variant:    Expected hardware variant.
        """
        try:
            self.__expected_hardware_type = device_info.HardwareType(hardware_type)
        except ValueError:
            raise ValueError("Invalid device hardware type: {}".format(hardware_type))
        
        if hardware_variant < 0:
            raise ValueError("Invalid device hardware variant: {}".format(hardware_variant))
        
        self.__expected_hardware_variant = hardware_variant
        
        self.clear()
    
    def clear(self) -> None:
        """
        Clear object.
        """
        self.magic = ""
        self.size = 0
        self.hardware_type = device_info.HardwareType.Unknown
        self.hardware_variant = 0
        self.state = ContainerState.Invalid
        self.reserved = bytearray()
    
    def parse(self, raw_data: bytearray) -> None:
        """
        Parse container header.
        
        :param raw_data:        Header raw data.
        
        :raises ValueError:     On parse error.
        """
        self.clear()
        
        if not isinstance(raw_data, bytearray):
            raise ValueError("Invalid type")

        if len(raw_data) != ContainerHeader.size:
            raise ValueError("Invalid header data size")
        
        idx = 0
        magic_data = raw_data[idx : (idx + 4)]
        magic_tmp = magic_data.decode('ascii')
        if magic_tmp != "CAL0":
            raise ValueError("Invalid field 'magic': '{}'".format(magic_tmp))
        self.magic = magic_tmp
        idx += 4
        
        size_data = raw_data[idx : (idx + 4)]
        size_tmp = int.from_bytes(size_data, byteorder='little', signed=False)
        size_empty_container = ContainerHeader.size + crc16_modbus.CRC16_SIZE
        if size_tmp <= size_empty_container:
            raise ValueError("Invalid field 'size': {}".format(size_tmp))
        self.size = size_tmp
        idx += 4
        
        hardware_type_data = raw_data[idx : (idx + 4)]
        hardware_type_tmp = int.from_bytes(hardware_type_data, byteorder='little', signed=False)
        if hardware_type_tmp != self.__expected_hardware_type:
            raise ValueError("Mismatch hardware type: expected: {}, get: {}"
                             .format(int(self.__expected_hardware_type), hardware_type_tmp))
        self.hardware_type = device_info.HardwareType(hardware_type_tmp)
        idx += 4
        
        hardware_variant_data = raw_data[idx : (idx + 4)]
        hardware_variant_tmp = int.from_bytes(hardware_variant_data, byteorder='little', signed=False)
        if hardware_variant_tmp != self.__expected_hardware_variant:
            raise ValueError("Mismatch hardware variant: expected: {}, get: {}"
                             .format(int(self.__expected_hardware_variant), hardware_variant_tmp))
        self.hardware_variant = hardware_variant_tmp
        idx += 4
        
        state_tmp = int(raw_data[idx])
        if state_tmp not in (ContainerState.Invalid, ContainerState.Valid):
            raise ValueError("Invalid field 'state': {}".format(state_tmp))
        self.state = ContainerState(state_tmp)
        idx += 1
        
        self.reserved = raw_data[idx : (idx + 16)]
        
    def to_string(self) -> str:
        """
        Convert to string.
        
        :return:     String.
        """
        s = "Magic: '{}'\n".format(self.magic)
        s += "Size: {}\n".format(self.size)
        s += "Hardware Type: {} ({})\n".format(self.hardware_type, self.hardware_type.name)
        s += "Hardware Variant: {}\n".format(self.hardware_variant)
        s += "State: {} ({})\n".format(self.state, self.state.name)
        s += "Reserved: '{}'".format(self.reserved.hex(' ').upper())

        return s

def get_empty_container_size() -> int:
    """
    Get empty container size.
    
    :return:     Empty container size.
    """
    return (ContainerHeader.size + crc16_modbus.CRC16_SIZE)

class CalibrationContainer:
    """
    Calibration data container.
    """
    def __init__(self,
                 hardware_type: device_info.HardwareType,
                 hardware_variant: int,
                 device_data_size: int) -> None:
        """
        Constructor.
        
        :param hardware_type:       Expected hardware type.
        :param hardware_variant:    Expected hardware variant.
        :param device_data_size:    Expected device calibration data size.
        """
        self.header = ContainerHeader(hardware_type, hardware_variant)
        
        if device_data_size < 1:
            raise ValueError("Invalid device data size")
        
        self.__expected_device_data_size = device_data_size
        
        self.clear()

    def clear(self) -> None:
        """
        Clear object.
        """
        self.header.clear()
        self.raw_data = bytearray()

    def get_device_data(self) -> bytearray:
        """
        Get device calibration data.
        
        :return:     Device calibration data.
        """
        device_data = bytearray()
        
        if len(self.raw_data) > 0:
            device_data = self.raw_data[ContainerHeader.size : (ContainerHeader.size + self.__expected_device_data_size)]
        
        return device_data

    def save(self, file_name: str) -> None:
        """
        Save raw data to binary file.
        
        :param file_name:    File name.
        """
        if len(self.raw_data) == 0:
            raise ValueError("Container is empty")    
        
        with open(file_name, "wb") as file:
            file.write(self.raw_data)
                
    def load(self, file_name: str) -> None:
        """
        Load raw data from binary file, parse and update self.
        
        :param file_name:       File name.
        
        :raises ValueError:     On parse error.
        """
        assert self.__expected_device_data_size > 0
        
        with open(file_name, "rb") as file:
            raw_data_size = get_empty_container_size() + self.__expected_device_data_size
            raw_data = bytearray(file.read(raw_data_size))
            self.parse(raw_data)

    def parse(self, raw_data: bytearray) -> None:
        """
        Parse raw data and build container object.
        
        :param raw_data:        Raw data from device.
        
        :raises ValueError:     On parse error.
        """
        self.clear()
        
        if not isinstance(raw_data, bytearray):
            raise ValueError("Invalid type")
        
        expected_container_size = ContainerHeader.size + self.__expected_device_data_size + crc16_modbus.CRC16_SIZE
        
        if len(raw_data) != expected_container_size:
            raise ValueError("Invalid raw data size: {}, expected: {}"
                             .format(len(raw_data), expected_container_size))
        
        if not self.__check_crc(raw_data):
            raise ValueError("Invalid CRC")
        
        header_data = raw_data[0 : ContainerHeader.size]
        self.header.parse(header_data)
        
        self.raw_data = raw_data.copy()
        
    def __check_crc(self, raw_data: bytearray) -> bool:
        """
        Check CRC16.
        
        :param raw_data:     Raw data from device.
        
        :return:      Check result.
        """
        crc_calculated_data_size = len(raw_data) - crc16_modbus.CRC16_SIZE
        crc_calculated = crc16_modbus.crc16_add_array(raw_data, crc_calculated_data_size)
        
        crc_field_idx = ContainerHeader.size + self.__expected_device_data_size
        crc_field_data = raw_data[crc_field_idx : (crc_field_idx + crc16_modbus.CRC16_SIZE)]
        crc_field = int.from_bytes(crc_field_data, byteorder='little', signed=False)
        
        is_valid = (crc_field == crc_calculated)
        
        return is_valid
        
    def to_string(self) -> str:
        """
        Convert to string.
        
        :return:     String.
        """
        s = "Header:\n"
        s += "{}\n".format(self.header.to_string())
        s += "Device data:\n"
        s += "'{}'".format(self.get_device_data().hex(' ').upper())

        return s
