"""
Host protocol
=============

Host binary protocol is designed for communication with peripheral devices over USB CDC Serial Port.

Communication procedure:
1. Master (host) send PDU to slave (device) with command code and optional data.
2. Slave execute command if PDU is valid.
3. Slave send answer PDU to host with answer code (= status or result) and optional data.

Protocol transport frame (PDU) structure:

```txt
+-----------------------------------------------------+------------------------+---------+
|              Header (5 bytes = 40 bits)             |         Payload        |  CRC16  |
+----------+----------+------------+---------+--------+------------------------+---------+
| PDU Type | Sequence | PDU Length | Command | Answer | Command or answer data |         |
|          | Counter  |            |  Code   |  Code  |                        | 16 bits |
| 2 bits   | 6 bits   | 16 bits    | 8 bits  | 8 bits |    1017 bytes max.     |         |
+----------+----------+------------+---------+--------+------------------------+---------+
|         1 B         |    2 B     |   1 B   |  1 B   |
+---------------------+------------+---------+--------+
```

PDU structure are identical for master and slave transmissions.\n
PDU length max. is 1024 bytes (header + payload + CRC).\n
CRC16 is MODBUS compatible and is calculated from all preceding bytes (header + payload).\n
Multibyte numbers are low endian.\n
Header bitfields coding: bits from left (from above table) are LSB.\n

PDU header fields:
- `PDU Type`: frame type: 0 = Command, 1 = Answer, 2 to 3 = reserved.
- `Sequence Counter`: frame number incremented by sender from 0 to 63 then overflow to 0.
- `PDU Length`: total frame length in bytes (header + payload + CRC).
- `Command Code`:
  - Master PDU: command code to execute by slave.
  - Slave PDU: copy of command code from master PDU.
- `Answer Code`:
  - Master PDU: always 0x00.
  - Slave PDU: answer code (= status or result) of executed command.
"""

import ctypes
import logging
import enum
from collections.abc import Callable

from . import crc16_modbus

HEADER_SIZE = 5
CHECKSUM_SIZE = 2
PDU_SIZE_MIN = HEADER_SIZE + CHECKSUM_SIZE
PDU_SIZE_MAX = 1024
PAYLOAD_SIZE_MAX = PDU_SIZE_MAX - PDU_SIZE_MIN
SEQUENCE_COUNTER_BITMASK = 0x3F

class PDU_Type(enum.IntEnum):
    Command = 0
    Answer = 1
    Reserved_1 = 2
    Reserved_2 = 3

class Header(ctypes.Structure):
    """
    Command/answer PDU header.
    """
    _pack_ = 1
    _fields_ = [
        ("pdu_type", ctypes.c_uint8, 2),
        ("sequence_counter", ctypes.c_uint8, 6),
        ("pdu_length", ctypes.c_uint16, 16),
        ("command", ctypes.c_uint8, 8),
        ("status", ctypes.c_uint8, 8)
    ]
    
    def to_string(self) -> str:
        return "pdu_type: {}, sequence_counter: {}, pdu_length: {}, command: {}, status: {}".format(
            self.pdu_type,
            self.sequence_counter,
            self.pdu_length,
            self.command,
            self.status)

class PDU:
    """
    Command/answer Protocol Data Unit (byte frame).
    """
    def __init__(self) -> None:
        self.header = Header()
        self.payload = bytearray()
        
    def to_bytes(self) -> bytearray:
        """
        Build byte frame.
        """
        if len(self.payload) > PAYLOAD_SIZE_MAX:
            raise ValueError("Invalid payload size: {}, limit: {}"
                             .format(len(self.payload), PAYLOAD_SIZE_MAX))
        
        self.header.pdu_length = HEADER_SIZE + len(self.payload) + CHECKSUM_SIZE
        pdu_bytes = bytearray(self.header) + self.payload
        crc = crc16_modbus.crc16_add_array(pdu_bytes, len(pdu_bytes))
        pdu_bytes += bytearray(crc.to_bytes(length=2, byteorder='little', signed=False))
        
        return pdu_bytes

class HostProtocolException(Exception):
    pass

type CallbackOnSuccess = Callable[[PDU], None]
type CallbackOnError = Callable[[HostProtocolException], None]

class CommandParser:
    """
    Command PDU parser.
    """
    class State(enum.IntEnum):
        Idle = 0
        Header = 1
        Payload = 2
        Checksum = 3
        EndSucces = 4
        EndError = 5
    
    def __init__(self,
                 callback_success: CallbackOnSuccess,
                 callback_error: CallbackOnError) -> None:
        """
        :param callback_success:    Callback on PDU parsing success.
        :param callback_error:      Callback on PDU parsing error.
        """
        self.callback_success = callback_success
        self.callback_error = callback_error
        
        self.reset()

    def reset(self) -> None:
        """
        Reset state.
        """
        self.state = self.State.Header
        self.pdu = PDU()
        self.field_offset = 0
        self.field = bytearray(PDU_SIZE_MAX)
        self.crc_calculated = crc16_modbus.CRC16_INITIAL_VALUE
        self.sequence_counter = 0
        self.exception = HostProtocolException()

    def parse(self, rx_data: bytearray) -> None:
        """
        Parse data chunk.
        """
        for byte in rx_data:
            #
            # Parser FSM.
            #
            match self.state:
                case self.State.Header:
                    self.field[self.field_offset] = byte
                    self.field_offset += 1
                    if self.field_offset == HEADER_SIZE:
                        self.field_offset = 0
                        self.pdu.header = Header.from_buffer_copy(self.field, 0)
                        
                        if (self.pdu.header.pdu_type != PDU_Type.Answer) or \
                            (self.pdu.header.pdu_length < PDU_SIZE_MIN) or \
                            (self.pdu.header.pdu_length > PDU_SIZE_MAX) or \
                            ((self.sequence_counter > 0) and \
                                (((self.sequence_counter + 1) & SEQUENCE_COUNTER_BITMASK) != self.pdu.header.sequence_counter)):
                            msg = "Invalid header: {}".format(self.pdu.header.to_string())
                            logging.error(msg)
                            self.exception = HostProtocolException(msg)
                            self.state = self.State.EndError
                        else:
                            self.sequence_counter = self.pdu.header.sequence_counter
                            self.crc_calculated = crc16_modbus.crc16_add_array(self.field, HEADER_SIZE, self.crc_calculated)
                            payload_size = self.pdu.header.pdu_length - PDU_SIZE_MIN
                            if payload_size > 0:
                                self.state = self.State.Payload
                            else:
                                self.state = self.State.Checksum
                    else:
                        # Continue header.
                        pass
                
                case self.State.Payload:
                    self.field[self.field_offset] = byte
                    self.field_offset += 1
                    payload_size = self.pdu.header.pdu_length - PDU_SIZE_MIN
                    if self.field_offset == payload_size:
                        self.pdu.payload = self.field[0 : self.field_offset]
                        self.field_offset = 0
                        self.crc_calculated = crc16_modbus.crc16_add_array(self.pdu.payload, len(self.pdu.payload), self.crc_calculated)
                        self.state = self.State.Checksum
                    else:
                        # Continue payload.
                        pass
                
                case self.State.Checksum:
                    self.field[self.field_offset] = byte
                    self.field_offset += 1
                    if self.field_offset == CHECKSUM_SIZE:
                        crc_bytes = self.field[0 : CHECKSUM_SIZE]
                        crc_in_pdu = int.from_bytes(crc_bytes, byteorder='little', signed=False)
                        if crc_in_pdu == self.crc_calculated:
                            self.state = self.State.EndSucces
                        else:
                            msg = f"Invalid CRC: frame: 0x{crc_in_pdu:X}, calculated: 0x{self.crc_calculated:X}"
                            logging.error(msg)
                            self.exception = HostProtocolException(msg)
                            self.state = self.State.EndError
                    else:
                        pass # Continue CRC16.
                
                case _:
                    assert False

            #
            # Parser state controller.
            #
            match self.state:
                case self.State.Header | self.State.Payload | self.State.Checksum:
                    # Continue parsing current field in PDU.
                    pass
                
                case self.State.EndSucces:
                    self.callback_success(self.pdu)
                    self.reset()
                    
                    # Continue parsing rest of chunk as new PDU.
                
                case self.State.EndError:
                    self.callback_error(self.exception)
                    self.reset()
                    
                    # Reject remaining invalid chunk.
                    return
                
                case _:
                    assert False

class HostProtocol():
    """
    Host protocol
    """
    def __init__(self,
                 callback_receive_success: CallbackOnSuccess,
                 callback_receive_error: CallbackOnError) -> None:
        """
        :param callback_receive_success:    Callback on receive valid answer PDU from device.
        :param callback_receive_error:      Callback on Rx PDU parsing error.
        """
        self.__command_parser = CommandParser(callback_receive_success, callback_receive_error)
        self.__command_sequence_counter = 0

    def reset(self) -> None:
        """
        Reset protocol.
        """
        self.__command_parser.reset()
        self.__command_sequence_counter = 0

    def parse(self, rx_data: bytearray) -> None:
        """
        Process Rx data.
        
        :param rx_data:    Rx data chunk.
        """
        self.__command_parser.parse(rx_data)

    def get_command_sequence_counter(self) -> int:
        """
        Get current value and increment command sequence counter.
        
        :return:     Sequence counter before increment.
        """
        current_sc = self.__command_sequence_counter
        self.__command_sequence_counter = (self.__command_sequence_counter + 1) & SEQUENCE_COUNTER_BITMASK
        
        return current_sc
