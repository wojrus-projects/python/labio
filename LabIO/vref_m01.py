"""
VREF_M01 basic functionality.
"""

import enum
import ctypes
import struct

from . import host_protocol
from . import device_basic
from . import device_basic_serial_number
from . import calibration_container

CALIBRATION_DATA_SIZE = 4

class CommandCode(enum.IntEnum):
    """
    Device specific commands (range 128-255).
    """
    Set_Voltage = 128
    Calibration_Process = 129
    Calibration_Data_Read = 130
    Calibration_Data_Write = 131

class StatusCode(enum.IntEnum):    
    """
    Device specific codes (range 128-255).
    """
    ADC_Calibration_Completed = 128
    ADC_Calibration_Invalid_Voltage = 129
    
class CalibrationStage(enum.IntEnum):
    Invalid = 0
    Stage_1_TestVoltageSet = 1
    Stage_2_TestVoltageMeasure = 2

class DeviceException(Exception):
    pass

class VREF_M01(device_basic_serial_number.DeviceBasicSerialNumber):
    """
    VREF_M01 Voltage Reference Module.
    """
    def __init__(self) -> None:
        super().__init__()
    
    def set_voltage(self, voltage_V: float) -> None:
        """
        Set voltage.
        
        :param voltage_V:       Output voltage in V, nominal range: 0.0 to +10.5 V.
        """
        pdu = host_protocol.PDU()
        pdu.header.pdu_type = host_protocol.PDU_Type.Command
        pdu.header.sequence_counter = self.host_protocol.get_command_sequence_counter()
        pdu.header.command = CommandCode.Set_Voltage
        pdu.header.status = 0
        pdu.payload = bytearray()
        
        voltage_V = float(voltage_V)
        if voltage_V < 0.0:
            voltage_V = 0.0
        
        fmt = struct.Struct('<f')
        pdu.payload += fmt.pack(ctypes.c_float(voltage_V).value)
        
        self.send_command_pdu(pdu)
        
        answer_pdu = self.receive_answer_pdu()
        self.__check_answer_pdu(answer_pdu, CommandCode.Set_Voltage, 0)
    
    def calibration_process(self,
                            calibration_stage: CalibrationStage,
                            voltage_measured_V: float) -> int:
        """
        Process device calibration.
        
        :param calibration_stage:       Calibration stage number.
        :param voltage_measured_V:      Stage 1: 0.0 V, stage 2: measured output voltage in V.
        
        :return:     Device answer code.
        """
        pdu = host_protocol.PDU()
        pdu.header.pdu_type = host_protocol.PDU_Type.Command
        pdu.header.sequence_counter = self.host_protocol.get_command_sequence_counter()
        pdu.header.command = CommandCode.Calibration_Process
        pdu.header.status = 0
        pdu.payload = bytearray(1)
        
        pdu.payload[0] = calibration_stage
        
        if calibration_stage == CalibrationStage.Stage_1_TestVoltageSet:
            pass
        elif calibration_stage == CalibrationStage.Stage_2_TestVoltageMeasure:
            voltage_measured_V = float(voltage_measured_V)
            fmt = struct.Struct('<f')
            pdu.payload += fmt.pack(ctypes.c_float(voltage_measured_V).value)
        else:
            raise ValueError("Invalid stage")
        
        self.send_command_pdu(pdu)
        
        answer_pdu = self.receive_answer_pdu()
        self.__check_answer_pdu(answer_pdu,
                                CommandCode.Calibration_Process,
                                0,
                                status_codes=(device_basic.StatusCode.Success,
                                              StatusCode.ADC_Calibration_Completed))

        return int(answer_pdu.header.status)
    
    def calibration_data_read(self) -> calibration_container.CalibrationContainer:
        """
        Read calibration data from NVM.
        
        :return:     Calibration data.
        """
        pdu = host_protocol.PDU()
        pdu.header.pdu_type = host_protocol.PDU_Type.Command
        pdu.header.sequence_counter = self.host_protocol.get_command_sequence_counter()
        pdu.header.command = CommandCode.Calibration_Data_Read
        pdu.header.status = 0
        
        self.send_command_pdu(pdu)
        
        answer_pdu = self.receive_answer_pdu()
        payload_length = calibration_container.get_empty_container_size() + CALIBRATION_DATA_SIZE
        self.__check_answer_pdu(answer_pdu, CommandCode.Calibration_Data_Read, payload_length)
        
        _, device_identification = self.get_device_info()
        
        container = calibration_container.CalibrationContainer(hardware_type=device_identification.hardware_type,
                                                               hardware_variant=device_identification.hardware_variant,
                                                               device_data_size=CALIBRATION_DATA_SIZE)
        container.parse(answer_pdu.payload)
        
        return container
    
    def calibration_data_write(self, calibration_data: calibration_container.CalibrationContainer) -> None:
        """
        Write calibration data to NVM.
        
        :param calibration_data:    Calibration data.
        """
        if not isinstance(calibration_data, calibration_container.CalibrationContainer):
            raise ValueError("Invalid type")
        
        raw_data_size = calibration_container.get_empty_container_size() + CALIBRATION_DATA_SIZE
        if len(calibration_data.raw_data) != raw_data_size:
            raise ValueError("Invalid size")
        
        pdu = host_protocol.PDU()
        pdu.header.pdu_type = host_protocol.PDU_Type.Command
        pdu.header.sequence_counter = self.host_protocol.get_command_sequence_counter()
        pdu.header.command = CommandCode.Calibration_Data_Write
        pdu.header.status = 0
        pdu.payload = calibration_data.raw_data
        
        self.send_command_pdu(pdu)
        
        answer_pdu = self.receive_answer_pdu()
        self.__check_answer_pdu(answer_pdu, CommandCode.Calibration_Data_Write, 0)
    
    type ValidStatusCodes = tuple[device_basic.StatusCode | StatusCode, ...]
    
    @staticmethod
    def __check_answer_pdu(answer_pdu: host_protocol.PDU,
                           command_code: int,
                           payload_length: int,
                           status_codes: (ValidStatusCodes | None) = None) -> None:
        """
        Check answer PDU.
        
        :param answer_pdu:      PDU from device.
        :param command_code:    Expected command code.
        :param status_codes:    Expected status codes.
        :param payload_length:  Expected payload length.
        """
        if answer_pdu is None:
            raise DeviceException("No response from device")
        
        if answer_pdu.header.command != command_code:
            raise DeviceException(f"Invalid command code in answer: {answer_pdu.header.command}")
    
        if len(answer_pdu.payload) != payload_length:
            raise DeviceException(f"Invalid payload length: {len(answer_pdu.payload)}")

        if status_codes is None:
            status_codes = (device_basic.StatusCode.Success,)
            
        if answer_pdu.header.status not in status_codes:
            raise DeviceException(f"Invalid status code: {answer_pdu.header.status}")
