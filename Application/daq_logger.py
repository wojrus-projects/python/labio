#
# Simple CLI application for data logging/streaming to HDF5 file.
# Supported devices: DAQ_M01, DAQ_M02.
#
# Author: Woj.Rus. <rwxrwx@interia.pl>
#
# License: MIT
#

import sys
import os
import logging
import threading
import psutil
import argparse
import platform
from typing import cast, Any

# Add relative path to LabIO package.
sys.path.append("..")

from LabIO import device_info
from LabIO import daq_m01
from LabIO import daq_m01_data_collector
from LabIO import daq_m02
from LabIO import daq_m02_data_collector

# Enable error logging.
logging.basicConfig(level=logging.ERROR, format='[%(asctime)s.%(msecs)03d] {%(filename)s:%(lineno)d} %(levelname)s - %(message)s', datefmt='%H:%M:%S')

# Enable handler to catch exceptions from threads.
def exception_hook(args: Any) -> None:
    print("Thread exception:")
    print(f"value:  {args.exc_value}")
    print(f"type:   {args.exc_type}")
    print(f"thread: {args.thread}")
    print("Terminate application")
    os._exit(1)

threading.excepthook = exception_hook

# Set application high priority.
process = psutil.Process(os.getpid())
system = platform.system()
try:
    if system == 'Windows':
        process.nice(psutil.REALTIME_PRIORITY_CLASS)
    else:
        process.nice(-15)
except psutil.AccessDenied:
    logging.warning("Can't change process priority, require elevated user rights")

class CLI_Arguments:
    """
    Application CLI arguments.
    """
    def __init__(self) -> None:
        self.hdf5_file_name = ""
        self.serial_port_name = ""
        
        self.hardware_type = device_info.HardwareType.Unknown
        self.sampling_frequency = 0
        self.sample_count = 0
        
        self.daq_m01_channels_state = [0] * daq_m01.ADC_CHANNEL_COUNT
        self.daq_m01_channels_gain = [daq_m01.PGA_GAIN_MIN] * daq_m01.ADC_CHANNEL_COUNT
        
        self.daq_m02_current_range = daq_m02.CurrentRange.Range_5_mA
        self.daq_m02_trigger_mode = daq_m02.TriggerMode.Disabled
        
    def to_string(self) -> str:
        s  = "HDF5 file: {}\n".format(self.hdf5_file_name)
        s += "Serial port: {}\n".format(self.serial_port_name)
        s += "Device type: {}\n".format(self.hardware_type)
        s += "Sampling frequency: {} Hz\n".format(self.sampling_frequency)
        s += "Sample count: {}\n".format(self.sample_count)
        
        if self.hardware_type == device_info.HardwareType.DAQ_M01:
            s += "Channels state: {}\n".format(self.daq_m01_channels_state)
            s += "Channels gain: {}".format(self.daq_m01_channels_gain)
        elif self.hardware_type == device_info.HardwareType.DAQ_M02:
            s += "Current range: {}\n".format(self.daq_m02_current_range.name)
            s += "Trigger mode: {}".format(self.daq_m02_trigger_mode.name)
        else:
            assert False
        
        return s

def get_cli_args() -> CLI_Arguments:
    """
    Get CLI arguments.
    
    Return:     CLI arguments.
    """
    parser = argparse.ArgumentParser(prog='daq_logger',
                                     description='Simple CLI application for data logging/streaming to HDF5 file.',
                                     epilog='')
    
    parser.add_argument('file',             type=str, help='HDF5 file name')
    parser.add_argument('port',             type=str, help='Serial port name')
    parser.add_argument('freq',             type=int, help='Sampling frequency in Hz (device dependent)')
    parser.add_argument('samples',          type=int, help='Sample count')
    
    subparsers = parser.add_subparsers(dest='device', help='Device type', required=True)
    
    parser_daq_m01 = subparsers.add_parser('daq_m01', help='DAQ_M01 (6x voltage channels, 24-bit)')
    parser_daq_m01.add_argument('channel',  type=str, help='Channels enable list: "S1,S2,S3,S4,S5,S6" where Sx is channel X state: {0,1}')
    parser_daq_m01.add_argument('gain',     type=str, help='Channels PGA gain list: "G1,G2,G3,G4,G5,G6" where Gx is channel X gain: {1,2,4,8,16,32,64,128}')
    
    parser_daq_m02 = subparsers.add_parser('daq_m02', help='DAQ_M02 (1x current channel, 16-bit)')
    parser_daq_m02.add_argument('range',    type=int, help='Current range: 5, 50, 500, 5000 mA', choices=daq_m02.ranges_mA)
    parser_daq_m02.add_argument('trigger',  type=int, help='Trigger mode: 0 = disabled, 1 = rising, 2 = falling, 3 = both edges', choices=list(map(int, daq_m02.TriggerMode)))
    
    args = parser.parse_args()
    
    #
    # Common arguments.
    #
    cli_arguments = CLI_Arguments()
    cli_arguments.hdf5_file_name = args.file
    cli_arguments.serial_port_name = args.port
    cli_arguments.sampling_frequency = args.freq
    cli_arguments.sample_count = args.samples
    
    if cli_arguments.sampling_frequency < 1:
        raise ValueError("Invalid sampling frequency")
    
    if cli_arguments.sample_count < 1:
        raise ValueError("Invalid sample count")
    
    #
    # Device dependent arguments.
    #
    if args.device == "daq_m01":
        cli_arguments.hardware_type = device_info.HardwareType.DAQ_M01
    elif args.device == "daq_m02":
        cli_arguments.hardware_type = device_info.HardwareType.DAQ_M02
    else:
        assert False
    
    if cli_arguments.hardware_type == device_info.HardwareType.DAQ_M01:
        daq_m01_channels_state = [int((int(s) > 0)) for s in args.channel.split(',')]
        daq_m01_channels_gain  = [int(s)            for s in args.gain.split(',')]
        
        if (len(daq_m01_channels_state) != daq_m01.ADC_CHANNEL_COUNT) or \
            (len(daq_m01_channels_gain) != daq_m01.ADC_CHANNEL_COUNT):
            raise ValueError("Invalid channels count (DAQ_M01 has {} channels)".format(daq_m01.ADC_CHANNEL_COUNT))

        for gain in daq_m01_channels_gain:
            if gain not in daq_m01.pga_gains:
                raise ValueError("Invalid PGA gain: {}".format(gain))

        cli_arguments.daq_m01_channels_state = daq_m01_channels_state
        cli_arguments.daq_m01_channels_gain = daq_m01_channels_gain
    elif cli_arguments.hardware_type == device_info.HardwareType.DAQ_M02:
        cli_arguments.daq_m02_current_range = daq_m02.current_to_range(args.range)
        cli_arguments.daq_m02_trigger_mode = daq_m02.TriggerMode(args.trigger)
    else:
        assert False
    
    return cli_arguments

def main(cli_args: CLI_Arguments) -> None:
    """
    Application main function.
    """
    print("Data logger v1.2")
    print("\nCLI arguments:")
    print(cli_args.to_string())
    
    type DeviceType = daq_m01_data_collector.DAQ_M01_DataCollector | daq_m02_data_collector.DAQ_M02_DataCollector | None
    
    device: DeviceType = None
    
    if cli_args.hardware_type == device_info.HardwareType.DAQ_M01:
        device = daq_m01_data_collector.DAQ_M01_DataCollector()
    elif cli_args.hardware_type == device_info.HardwareType.DAQ_M02:
        device = daq_m02_data_collector.DAQ_M02_DataCollector()
    else:
        assert False

    try:
        print("\nConnect to {}".format(cli_args.serial_port_name))
        device.connect(cli_args.serial_port_name)
        
        #
        # Check device.
        #
        print("\nRead device version")
        _, device_identification = device.get_device_info()
        print(device_identification.to_string())
        
        if device_identification.hardware_type != cli_args.hardware_type:
            raise ValueError(f"Unsupported device: {device_identification.hardware_type}")
        
        #
        # Check calibration.
        #
        print("\nRead calibration status")
        acquisition_status = device.acquisition_get_status()
        if acquisition_status.calibration_is_valid:
            print("Calibration is valid")
        else:
            print("Warning: device is uncalibrated")
        
        #
        # Start acquisition.
        #
        print("\nStart sampling")
        try:
            if cli_args.hardware_type == device_info.HardwareType.DAQ_M01:
                device = cast(daq_m01_data_collector.DAQ_M01_DataCollector, device)
                device.read_data(enable_output_numpy_array=False,
                                 enable_output_hdf5_file=True,
                                 hdf5_file_name=cli_args.hdf5_file_name,
                                 adc_sampling_frequency=cli_args.sampling_frequency,
                                 adc_sample_count_max=cli_args.sample_count,
                                 adc_channel_state=cli_args.daq_m01_channels_state,
                                 adc_pga_gain=cli_args.daq_m01_channels_gain,
                                 show_progress=True)
            elif cli_args.hardware_type == device_info.HardwareType.DAQ_M02:
                device = cast(daq_m02_data_collector.DAQ_M02_DataCollector, device)
                device.read_data(enable_output_numpy_array=False,
                                 enable_output_hdf5_file=True,
                                 hdf5_file_name=cli_args.hdf5_file_name,
                                 trigger_mode=cli_args.daq_m02_trigger_mode,
                                 adc_current_range=cli_args.daq_m02_current_range,
                                 adc_sampling_frequency=cli_args.sampling_frequency,
                                 adc_sample_count_single_acquisition=cli_args.sample_count,
                                 adc_sample_count_total=cli_args.sample_count,
                                 show_progress=True)
            else:
                assert False
        except KeyboardInterrupt:
            print("\nStop process")
            sys.exit(1)
        
        #
        # Check acquisition status.
        #
        acquisition_status = device.acquisition_get_status()
        
        if cli_args.hardware_type == device_info.HardwareType.DAQ_M01:
            acquisition_status = cast(daq_m01.AcquisitionStatus, acquisition_status)
            counter_buffer_overflow = acquisition_status.counter_buffer_overflow
        elif cli_args.hardware_type == device_info.HardwareType.DAQ_M02:
            acquisition_status = cast(daq_m02.AcquisitionStatus, acquisition_status)
            counter_buffer_overflow = acquisition_status.counter_buffer_overflow_acquisition
        else:
            assert False
        
        if counter_buffer_overflow > 0:
            percent = 100 * counter_buffer_overflow / cli_args.sample_count
            print("Warning: DAQ buffer overflow, lost {} samples ({} %)".format(
                counter_buffer_overflow, percent))
        
        device.disconnect()
        
        print("Data read completed")
        
    except Exception as exc:
        print(exc)

if __name__ == '__main__':
    try:
        cli_args = get_cli_args()
    except Exception as exc:
        print(exc)
        sys.exit(1)
        
    main(cli_args)
