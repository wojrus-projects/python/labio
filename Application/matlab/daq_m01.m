%
% Open HDF5 file from DAQ_M01 and calculate THD, SNR, SINAD, SFDR, ENOB and FFT.
%
% Matlab R2023a
%

file_name = 'daq_m01.hdf5';
dataset_name = '/DAQ_M01/Measurements';

info = h5info(file_name, dataset_name)
h5disp(file_name)

% Get all data from dataset.
adc_data = h5read(file_name, dataset_name);
adc_data_size = size(adc_data);
adc_data = transpose(adc_data);

Fs = double(h5readatt(file_name, dataset_name, 'ADC sampling frequency'));

% Calculate THD, SNR, SINAD, SFDR, ENOB (all ADC channels).

adc_thd=[];
adc_snr=[];
adc_snr_noise_power=[];
adc_sinad=[];
adc_sinad_total_noise_and_harmonic_distortion_power=[];
adc_sfdr=[];
adc_enob=[];

for adc_channel_data = adc_data
    adc_thd(end+1) = thd(adc_channel_data, Fs, 5);

    [adc_channel_snr,adc_channel_noise_power] = snr(adc_channel_data, Fs, 5);
    adc_snr(end+1) = adc_channel_snr;
    adc_snr_noise_power(end+1) = adc_channel_noise_power;

    [adc_channel_sinad,adc_channel_sinad_total_noise_and_harmonic_distortion_power] = sinad(adc_channel_data, Fs);
    adc_sinad(end+1) = adc_channel_sinad;
    adc_sinad_total_noise_and_harmonic_distortion_power(end+1) = adc_channel_sinad_total_noise_and_harmonic_distortion_power

    adc_sfdr(end+1) = sfdr(adc_channel_data, Fs);

    % TODO: add amplitude normalization (ref. Analog Devices MT-003).
    adc_enob(end+1) = (adc_channel_sinad - 1.76) / 6.02;
end

adc_thd
adc_snr
adc_snr_noise_power
adc_sinad
adc_sinad_total_noise_and_harmonic_distortion_power
adc_sfdr
adc_enob

% Calculate FFT (all ADC channels).
L = adc_data_size(2);
t = (0:L-1)/Fs;
f = linspace(0, Fs/2, L/2+1);

FFT = abs(fft(adc_data)/L);
FFT(2:L/2) = 2*FFT(2:L/2);

layout = tiledlayout('vertical');
layout.TileSpacing = 'compact';
layout.Padding = 'compact';

nexttile
for data = adc_data
    plot(t, data);
    hold on
end
hold off

title("DAQ-M01 samples")
xlabel('Time (s)');
ylabel('Voltage (V)');
grid on

nexttile
for data = FFT
    plot(f, data(1:L/2+1));
    hold on
end
hold off

title("DAQ-M01 FFT (linear V)")
xlabel('Frequency (Hz)');
ylabel('Voltage (V)');
grid on

nexttile
for data = FFT
    plot(f, mag2db(data(1:L/2+1)));
    hold on
end
hold off

title("DAQ-M01 FFT (dBV)")
xlabel('Frequency (Hz)');
ylabel('Voltage (dBV)');
grid on
