%
% Open HDF5 file from DAQ_M02 and calculate THD, SNR, SINAD, SFDR, ENOB and FFT.
%
% Matlab R2023a
%

file_name = 'daq_m02.hdf5';
dataset_name = '/DAQ_M02/Measurements';

info = h5info(file_name, dataset_name)
h5disp(file_name)

% Get all data from dataset.
adc_data = h5read(file_name, dataset_name);
adc_data_size = size(adc_data)
adc_data = transpose(adc_data);

Fs = double(h5readatt(file_name, dataset_name, 'ADC sampling frequency'))
range = double(h5readatt(file_name, dataset_name, 'ADC current range'))

% Calculate THD, SNR, SINAD, SFDR, ENOB.

[adc_thd,harm_pow,harm_freq]=thd(adc_data, Fs, 10)
[adc_snr,adc_snr_noise_power]=snr(adc_data)
[adc_sinad,adc_sinad_total_noise_and_harmonic_distortion_power]=sinad(adc_data, Fs)
adc_sfdr=sfdr(adc_data, Fs)
% TODO: add amplitude normalization (ref. Analog Devices MT-003).
adc_enob = (adc_sinad - 1.76) / 6.02

% Calculate FFT.
L = adc_data_size(1);
t = (0:L-1)/Fs;
f = linspace(0, Fs/2, L/2+1);

FFT = abs(fft(adc_data)/L);
FFT(2:L/2) = 2*FFT(2:L/2);

layout = tiledlayout('vertical');
layout.TileSpacing = 'compact';
layout.Padding = 'compact';

nexttile
plot(t, adc_data);

title("DAQ-M02 samples")
xlabel('Time (s)')
ylabel('Current (A)')
grid on

% TODO: uncomment to enable linear FFT.
%nexttile
%plot(f, FFT(1:L/2+1));

%title("DAQ-M02 FFT (linear)")
%xlabel('Frequency (Hz)')
%ylabel('Current (A)')
%grid on

% Normalize to measurement range
range_dB=mag2db(range)
FFT_dB=mag2db(FFT(1:L/2+1)) - range_dB;

nexttile
plot(f, FFT_dB);

title("DAQ-M02 FFT (dB of range)")
xlabel('Frequency (Hz)')
ylabel('Current (dB of range)')
grid on
