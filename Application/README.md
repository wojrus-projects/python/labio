# Lista programów

1. [daq_logger](#daq_logger)
2. [daq_plot](#daq_plot)

# daq_logger

Program odczytuje dane z wejść analogowych DAQ_M01 lub DAQ_M02 i zapisuje do pliku w formacie [HDF5](https://www.hdfgroup.org/solutions/hdf5/).<br>
Pliki HDF5 można analizować w Matlab, Octave itp. Przykładowe skrypty Matlab do wizualizacji danych:
- [matlab/daq_m01.m](matlab/daq_m01.m)
- [matlab/daq_m02.m](matlab/daq_m02.m)

Do celów testowych można użyć programu `HDFView` (https://www.hdfgroup.org/download-hdfview).

Składnia podstawowych argumentów CLI:

```
> python daq_logger.py -h

usage: daq_logger [-h] file port freq samples {daq_m01,daq_m02} ...

Simple CLI application for data logging/streaming to HDF5 file.

positional arguments:
  file               HDF5 file name
  port               Serial port name
  freq               Sampling frequency in Hz (device dependent)
  samples            Samples count
  {daq_m01,daq_m02}  Device type
    daq_m01          DAQ_M01 (6x voltage channels, 24-bit)
    daq_m02          DAQ_M02 (1x current channel, 16-bit)

options:
  -h, --help         show this help message and exit
```

Dodatkowe argumenty dla DAQ_M01:

```
usage: daq_logger file port freq samples daq_m01 channel gain

positional arguments:
  channel     Channels enable list: "S1,S2,S3,S4,S5,S6" where Sx is channel X state: {0,1}
  gain        Channels PGA gain list: "G1,G2,G3,G4,G5,G6" where Gx is channel X gain: {1,2,4,8,16,32,64,128}
```

Dodatkowe argumenty dla DAQ_M02:

```
usage: daq_logger file port freq samples daq_m02 range trigger

positional arguments:
  range       Current range: 5, 50, 500, 5000 mA
  trigger     Trigger mode: 0 = disabled, 1 = rising, 2 = falling, 3 = both edges
```

Przykłady (Windows 10):

```bat
python daq_logger.py daq_m01.hdf5 COM3 32000 64000 daq_m01 "1,1,1,1,1,1" "2,2,2,16,16,16"
```
```bat
python daq_logger.py daq_m02.hdf5 COM4 32000 64000 daq_m02 50 0
```

# daq_plot

Program odczytuje dane z wejść analogowych DAQ_M01 lub DAQ_M02 i wyświetla wykres czasowy i FFT.

Dane są automatycznie zapisywane do plików w formacie [HDF5](https://www.hdfgroup.org/solutions/hdf5/) oraz Numpy [NPZ](https://numpy.org/doc/stable/reference/generated/numpy.savez_compressed.html).

Składnia argumentów CLI:

```
> python daq_plot.py -h

usage: daq_plot [-h] port freq samples {daq_m01,daq_m02} ...

Simple CLI application for analog data plotting.

positional arguments:
  port               Serial port name
  freq               Sampling frequency in Hz (device dependent)
  samples            Sample count
  {daq_m01,daq_m02}  Device type
    daq_m01          DAQ_M01 (6x voltage channels, 24-bit)
    daq_m02          DAQ_M02 (1x current channel, 16-bit)

options:
  -h, --help         show this help message and exit
```

Dodatkowe argumenty dla DAQ_M01:

```
usage: daq_plot port freq samples daq_m01 channel gain

positional arguments:
  channel     Channels enable list: "S1,S2,S3,S4,S5,S6" where Sx is channel X state: {0,1}
  gain        Channels PGA gain list: "G1,G2,G3,G4,G5,G6" where Gx is channel X gain: {1,2,4,8,16,32,64,128}
```

Dodatkowe argumenty dla DAQ_M02:

```
usage: daq_plot port freq samples daq_m02 range trigger

positional arguments:
  range       Current range: 5, 50, 500, 5000 mA
  trigger     Trigger mode: 0 = disabled, 1 = rising, 2 = falling, 3 = both edges
```

Przykłady (Windows 10):

```bat
python daq_plot.py COM3 32000 64000 daq_m01 "1,1,1,1,1,1" "2,2,2,16,16,16"
```
```bat
python daq_plot.py COM4 32000 64000 daq_m02 50 0
```

# Licencja

MIT
