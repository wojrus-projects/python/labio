cls
echo off

set FLAG=--follow-untyped-imports

mypy %FLAG% Application
mypy %FLAG% LabIO

pause
